//
//  ConnectionManager.swift
//  PayEx
//
//  Created by Anton Zhigalov on 04.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation

protocol ConnectionManager {
    func establishConnection(success: @escaping (Any?) -> Void,
                             failure: @escaping (ConnectionError) -> Void)

    func isConnected() -> Bool
}
