//
//  ConnectionErrors.swift
//  PayEx
//
//  Created by Anton Zhigalov on 05.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation

public enum ConnectionError: Error {
    case connectionLost
    case terminalNotFound
    case serviceStartingFailed
    case connectionValidationFailed

    var localizedDescription: String {
        switch self {
        case .connectionLost:
            return NSLocalizedString("Connection lost", comment: "")
        case .terminalNotFound:
            return NSLocalizedString("Terminal not found", comment: "")
        case .serviceStartingFailed:
            return NSLocalizedString("Failed to start PCL service", comment: "")
        case .connectionValidationFailed:
            return NSLocalizedString("Could not validate connection", comment: "")
        }
    }
}
