//
//  ConnectionSteps.swift
//  PayEx
//
//  Created by Anton Zhigalov on 05.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation

public enum ConnectionSteps {
    case setActiveTerminal
    case setConnectionParameters
    case startConnectionService
    case detectTerminalConnection
    case validateConnectionService
    case unknown
}
