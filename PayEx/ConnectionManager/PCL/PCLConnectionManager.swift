//
//  PCLConnectionManager
//  PayEx
//
//  Created by Anton Zhigalov on 04.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation
import iSMP

//The connection process consists of some steps,
//you can add new steps if neccessary, just don't forget to implement the logic for them in executeStep(_ step: ConnectionSteps)
//or remove existing ones, change their orders etc.

//When a step fails, the whole connection process stops

//Connection is established when all the steps has been passed successful

class PCLConnectionManager: NSObject, ConnectionManager, ICPclServiceDelegate {
    //List of steps to get the connection established
    private let connectionSteps: [ConnectionSteps] = [.setActiveTerminal,
                                                     .setConnectionParameters,
                                                     .startConnectionService,
                                                     .detectTerminalConnection,
                                                     .validateConnectionService]
    //Closures to call on the client's side
    private var success: ((Any?) -> Void)!
    private var failure: ((ConnectionError) -> Void)!

    //Current active terminal
    //Perhaps it's not even neccesary to keep it, because you can get it from PCLService, but
    //If you are going to store the last used terminal in userDefaults, you will need it i think
    private var terminal: ICTerminal?

    //It might be a constant as well
    private var connectionOptions: ICSSLParameters?

    // MARK: - ConnectionManager
    func establishConnection(success: @escaping (Any?) -> Void,
                             failure: @escaping (ConnectionError) -> Void) {
        self.success = success
        self.failure = failure

        //Starting with the first step
        if let firstStep = self.connectionSteps.first {
            executeStep(firstStep)
        }
    }

    func isConnected() -> Bool {
        return isServiceReady()
    }

    // MARK: - ICPclServiceDelegate
    func notifyConnection(_ sender: ICPclService!) {
        finishStep(.detectTerminalConnection, data: sender)
    }

    func notifyDisconnection(_ sender: ICPclService!) {
        finishStep(.detectTerminalConnection, data: ConnectionError.connectionLost)
    }

    //Delegate method to see logs in the console
    //Remove it if necessary
    func pclLogEntry(_ message: String!, withSeverity severity: Int32) {
        print("[\(ICPclService.severityLevelString(Int32(severity)) ?? "??")] \(message ?? "??")")
    }

    // MARK: - Private
    private func executeStep(_ step: ConnectionSteps) {
        switch step {
        case .setActiveTerminal: //PCLService can start only when you pass a terminal as an argument
            if let terminal = getNeededTerminal() {
                self.terminal = terminal

                finishStep(.setActiveTerminal, data: terminal)
            } else {
                finishStep(.setActiveTerminal, data: ConnectionError.terminalNotFound)
            }

        case .setConnectionParameters:
            self.connectionOptions = getConnectionOptions()

            finishStep(.setConnectionParameters, data: self.connectionOptions)

        case .startConnectionService:
            if let terminal = self.terminal, let options = self.connectionOptions {
                startService(terminal: terminal, options: options)

                if isServiceReady() {
                    finishStep(.startConnectionService, data: ICPclService.shared().getState())

                    break
                }
            }
            finishStep(.startConnectionService, data: ConnectionError.serviceStartingFailed)

        case .validateConnectionService:
            if isServiceReady() {
                finishStep(.validateConnectionService, data: self.terminal)
            } else {
                finishStep(.validateConnectionService, data: ConnectionError.connectionValidationFailed)
            }

        default:
            break
        }
    }

    //Finish the curent connection step and start the next one
    //If it was the last step and passed successful - call successs closure
    private func finishStep(_ step: ConnectionSteps, data: Any?) {
        if let error = data as? ConnectionError {
            self.failure(error)
        } else {
            if let nextStep = self.connectionSteps.next(item: step) {
                executeStep(nextStep)
            } else { //It was the final initiating step
                self.success(self.terminal)
            }
        }
    }

    // MARK: Helpers
    //PCLService will return all available terminals. It can be wifi and bluetooth terminals as well
    private func getAvailableTerminals() -> [ICTerminal]? {
        return ICPclService.shared().getAvailableTerminals()
    }

    //Setting the first found terminal as the active one
    //You can also restore a terminal from userDefaults for example
    private func getNeededTerminal() -> ICTerminal? {
        guard let terminals = getAvailableTerminals(),
            let terminal = terminals.first else {
                return nil
        }

        return terminal
    }

    //Add SSL settings if you support it
    private func getConnectionOptions() -> ICSSLParameters {
        let initValue = ICSSLParameters()
        initValue.isSSL = false
        //initValue.sslCertificateName = "serverb"
        //initValue.sslCertificatePassword = "coucou"

        return initValue
    }

    //Start PCLService
    //PCLService requires a terminal to start.
    //If you start it, when the iphone is removed or the terminal is OFF, it won't be initiated correct and
    //you won't get ICPclServiceDelegate notifications (e.g. connect and diconnect)
    private func startService(terminal: ICTerminal, options: ICSSLParameters) {
        //Otherwise it might work strange when reconnecting
        //After calling stop(), notifyDisconnection(_ sender: ICPclService!) will be fired,
        //if the service was run
        ICPclService.shared().stop()

        ICPclService.shared().delegate = self
        ICPclService.shared().start(with: terminal, andSecurity: options)
    }

    //Get the status and try to get some data from the terminal.
    //You can't rely only on the service state (.getState()), because it can be correct,
    //but no data is returned from the terminal

    //Noticed a strange thing: sometimes it takes a few seconds to get data from the terminal,
    //and CPU is being loaded up to 100%
    private func isServiceReady() -> Bool {
        let state = ICPclService.shared().getState()
        let additionalCheck = ICPclService.shared().getTerminalInfo()

        return (state == PCL_SERVICE_STARTED || state == PCL_SERVICE_CONNECTED) && additionalCheck.serialNumber >= 0
    }
}
