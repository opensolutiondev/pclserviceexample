//
//  TestViewControllerConfigurator.swift
//  PayEx
//
//  Created by Anton Zhigalov on 05.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import UIKit

class TestViewControllerConfigurator: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var testViewController: TestViewController!

    override func awakeFromNib() {

        configure()
    }

    private func configure() {
        let connectionManager = PCLConnectionManager()
        let communicationManager = PayexJSONCommunicationManager()

        self.testViewController.connectionManager = connectionManager
        self.testViewController.communicationManager = communicationManager
    }
}
