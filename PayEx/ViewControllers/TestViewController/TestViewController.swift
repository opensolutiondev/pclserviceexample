//
//  ViewController.swift
//  PayEx
//
//  Created by Anton Zhigalov on 28.04.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import UIKit

private let kPingInterval = 5
private let kSessionLogSegueID = "toSessionLogViewController"

enum Package {
    case sent(Message)
    case received(Message)
    case error(CommunicationError)
    case finished
}

class TestViewController: UIViewController {
    //UI stuff
    @IBOutlet weak var connectionStatusLabel: UILabel!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var communicationResultLabel: UILabel!

    //Responsible for terminal connection/disconnection
    var connectionManager: ConnectionManager!

    //JSON communication
    var communicationManager: TerminalCommunicationManager!

    private var isPinging = false
    private var pingTimer: Timer?

    //Session log. Stores all communication packages after app started
    private var sessionLog = [Package]()

    override func viewDidLoad() {
        super.viewDidLoad()

        //Try to connect on startup
        connect()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? SessionLogViewController {
            viewController.sessionLog = self.sessionLog
        }
    }

    // MARK: - Actions
    @IBAction func reconnectButtonDidTap(_ sender: Any) {
        connect()
    }

    @IBAction func pingSwitchDidChange(_ sender: Any) {
        self.isPinging = !self.isPinging

        ping()
    }

    @IBAction func sendTransactionButtonDidTap(_ sender: Any) {
        if !self.communicationManager.isBusy() {
            let message = Message(messageType: "init_transaction",
                                  data: ["amount_authorized": 1,
                                         "transaction_type": "purchase",
                                         "merchant_id": "20000088"])

            self.sessionLog.append(Package.sent(message))

            self.communicationManager.sendMessage(message: message,
                                                  success: { [unowned self] (msgReceived) in
                                                    self.communicationResultLabel.text = "\(msgReceived)"

                                                    self.sessionLog.append(Package.received(msgReceived))

                                                    //It's only for testing purposes. Handling communications within an operation
                                                    //(e.g. purchase, reversal etc) should be entrusted to a special class
                                                    if msgReceived.messageType == "ok" || msgReceived.messageType == "not_ok" {
                                                        self.communicationManager.disconnect()
                                                    }
                }, failure: { [unowned self] error in
                    self.communicationResultLabel.text = error.localizedDescription

                    self.sessionLog.append(Package.error(error))
                }, finished: {
                    self.communicationResultLabel.text = "Operation finished"

                    self.sessionLog.append(Package.finished)
            })
        }
    }

    @IBAction func sessionLogButtonDidTap(_ sender: Any) {
        self.performSegue(withIdentifier: kSessionLogSegueID, sender: self)
    }

    @IBAction func clearLogButtonDidTap(_ sender: Any) {
        self.sessionLog.removeAll()
    }

    // MARK: - Private
    private func connect() { //Based on PCLService
        self.activityIndicator.startAnimating()

        self.connectionManager.establishConnection(success: { [unowned self] (response) in //E.g. terminal object
            self.activityIndicator.stopAnimating()

            self.connectionStatusLabel.text = "Connected"
            self.dataLabel.text = "\(response ?? "")"
        }, failure: { [unowned self] _ in //it returns the error to understand the reason of failure
            self.activityIndicator.stopAnimating()

            self.connectionStatusLabel.text = "Disconnected"
            self.dataLabel.text = nil
        })
    }

    //Trying to establish connection every 5 seconds, if the connection is not established yet
    private func ping() {
        if self.isPinging {
            self.pingTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval(kPingInterval),
                                 repeats: true,
                                 block: { [unowned self] _ in
                                    if !self.connectionManager.isConnected() {
                                        self.connect()
                                    }
            })

            self.pingTimer?.fire()
        } else {
            self.pingTimer?.invalidate()
            self.pingTimer = nil
        }
    }
}
