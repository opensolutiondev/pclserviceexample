//
//  SessionLogViewController.swift
//  PayEx
//
//  Created by Anton Zhigalov on 13.06.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import UIKit

private let kCellReuseID = "cell-id"

class SessionLogViewController: UIViewController {
    @IBOutlet weak var table: UITableView!

    var sessionLog: [Package]!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.table.register(UITableViewCell.self, forCellReuseIdentifier: kCellReuseID)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Actions
}

extension SessionLogViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sessionLog.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kCellReuseID) {
            let package = self.sessionLog[indexPath.row]

            cell.textLabel?.numberOfLines = 0

            switch package {
            case .sent(let message):
                cell.textLabel?.text = "SENT: " + message.serialize()
            case .received(let message):
                cell.textLabel?.text = "RECEIVED: " + message.serialize()
            case .error(let error):
                cell.textLabel?.text = "ERROR: " + error.localizedDescription
            case .finished:
                cell.textLabel?.text = "OPERATION FINISHED"
            }

            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
