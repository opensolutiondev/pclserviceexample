//
//  SessionLogViewControllerConfigurator.swift
//  PayEx
//
//  Created by Anton Zhigalov on 05.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import UIKit

class SessionLogViewControllerConfigurator: NSObject {

    //Connect with object on storyboard
    @IBOutlet weak var sessionLogViewController: SessionLogViewController!

    override func awakeFromNib() {

        configure()
    }

    private func configure() {
    }
}
