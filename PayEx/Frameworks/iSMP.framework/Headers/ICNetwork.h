//
//  ICNetwork.h
//  PCL Library
//
//  Created by Christophe Fontaine on 21/06/10.
//  Copyright 2010 Ingenico. All rights reserved.
//

/*!
    @file       ICNetwork.h
    @brief      Header file for ICNetwork class and related data structures and protocols
*/


#import <Foundation/Foundation.h>
#import "ICISMPDevice.h"



@protocol ICNetworkDelegate;


/*!
    @defgroup   ICNetworkGroup  Networking
    @brief      All networking classes and protocols provided by the PCL library
    @{
*/

/*!
    @brief    Companion Network Management
    <p>generic Companion's network managment</p>
*/
@interface ICNetwork : ICISMPDevice <NSStreamDelegate>
{
	NSMutableArray	* openConnections;                          /**< An array of all open connections */
	
	NSOperationQueue	* spmToHostWriteOperations;             /**< An operation queue on which the write operations are scheduled */
}


/*!
    @defgroup   ICNetworkInitialization ICNetwork Initialization
    @ingroup    ICNetworkGroup
    @brief      Steps to initialize the ICNetwork object
    <p>
        In order to initialize an @ref ICNetwork object, do as following:
        <ul>
            <li>Retain the shared ICNetwork instance returned by @ref sharedChannel</li>
            <li>Set the @ref networkDelegate property of ICNetwork to a valid object implementing the @ref ICISMPDeviceDelegate and @ref ICNetworkDelegate protocols</li>
            <li>Check the @ref isAvailable property of the ICNetwork to determine if the network channel is open and ready</li>
        </ul>
    </p>
    @{
*/

/*!  
    @anchor  sharedChannel
	@brief   Returns the unique shared instance of ICNetwork
	<p>
        The object returned by this method is autoreleased and should be retained in order to be kept alive.
    </p>
	@result		A pointer to the shared instance
*/
+(id) sharedChannel;


/*!   
    @anchor  networkDelegate
    @brief   The delegate object that should implement the @ref ICNetworkDelegate protocol
    <p>
        By implementing the protocol, the delegate is notified with @ref ICNetwork's events
    </p>
*/
@property (nonatomic, weak) id<ICISMPDeviceDelegate,ICNetworkDelegate> delegate;

/*! 
    @}  //ICNetworkInitialization
*/

@end


/*!
    @anchor     ICNetworkDelegate
	@brief      The ICNetwork Channel's delegate methods
	<p>
        These methods should be implemented by the @ref ICNetwork's delegate to subscribe to its events
    </p>
*/
@protocol ICNetworkDelegate
@optional

/*!
    @defgroup ICNetworkCallbacks    Network Notifications
    @ingroup  ICNetworkGroup
    @brief    Notifications of network connection changes
    <p>These callbacks are defined within @ref ICNetworkDelegate and should be implemented by the delegate of @ref ICNetwork</p>
    @{
*/

/*!
	@brief   Log event handler to be implemented by the delegate of the @ref ICNetwork object
	@param		data The logged network data
	@param		isIncoming YES if the data comes to the iDevice, NO otherwise.
*/
-(void)networkData:(NSData *)data incoming:(BOOL)isIncoming;

/*! 
    @brief   Method called when the network channel tries to connect to a remote host
    <p>
        ICNetwork delegate may implement this method to be notified when the Companion attempts to open a connection to a remote server.
    </p>
    @param		host The host name or IP address
    @param		port The destination port
 */
-(void)networkWillConnectToHost:(NSString *)host onPort:(NSUInteger)port;


/*!  
    @brief   Method called when a connection to a remote host is opened
    <p>
        ICNetwork delegate may implement this method to be notified when a network connection is established between the Companion and a remote server
    </p>
    @param		host The host name or IP address
    @param		port The destination port
*/
-(void)networkDidConnectToHost:(NSString *)host onPort:(NSUInteger)port;


/*!
    @brief   Method called when the network channel failed to connect to a remote host
    <p>
        ICNetwork delegate may implement this method to be notified of connection failures to a remote server.
    </p>
    @param		host The host name or IP address
    @param		port The destination port
*/
-(void)networkFailedToConnectToHost:(NSString *)host onPort:(NSUInteger)port;


/*!
    @brief   Method called when the Companion is disconnected from a remote host
    <p>
        ICNetwork delegate may implement this method to be notified when the Companion is disconnected from a remote server.
    </p>
    @param		host The host name or IP address
    @param		port The destination port
*/
-(void)networkDidDisconnectFromHost:(NSString *)host onPort:(NSUInteger)port;


/*!
    @brief   Method called if network errors occur during communication with a remote host
    <p>
        ICNetwork delegate may implement this method to be notified if there are some network errors when communicating with the remote host.
    </p>
    @param		host The host name or IP address
    @param		port The destination port
*/
-(void)networkDidReceiveErrorWithHost:(NSString *)host andPort:(NSUInteger)port;

/*! 
    @}  //ICNetworkCallbacks
*/

@end

/*! 
    @}  //ICNetworkGroup
*/


