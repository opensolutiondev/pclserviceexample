//
//  ICAdministration+iBP.h
//  PCL
//
//  Created by Hichem Boussetta on 02/01/12.
//  Copyright (c) 2012 Ingenico. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ICAdministration.h"

/*!
    @file       ICAdministration+iBP.h
    @brief      Header file for ICAdministration's Bluetooth printer category
    <p>
        This file contains the definitions of:
        <ul>
            <li>ICAdministration_iBP category</li>
        </ul>
    </p>
 */

/*!
 @defgroup   ICAdministrationBluetoothPrinter Performing printing
 @ingroup    ICAdministrationGroup
 @brief      All methods and structures necessary to perform text or bitmap printing on the Bluetooth Printer paired with the Companion.
             A printing session must already be opened before calling all these methods, otherwise the call returns immediately with a failure value.
 @{
 */


/*!
    @anchor eiBPResult
    An enumeration of the return constants of the iBP API
*/
enum eiBPResult {
    iBPResult_OK,                               /**< Request Success */
    iBPResult_KO,                               /**< Request Failure due to a wrong parameters passed to the Companion */
    iBPResult_TIMEOUT,                          /**< Request timeout meaning that no response came from the terminal */
    iBPResult_ISMP_NOT_CONNECTED,               /**< Failure because the iDevice and the Companion are not synchronized */
    iBPResult_PRINTER_NOT_CONNECTED,                 /**< Request failure due to the printer not being open. @ref iBPOpenPrinter should be called to recover from this error */
    iBPResult_INVALID_PARAM,                    /**< Request failure because the parameters passed to the API are irrelevant (null parameter) */
    iBPResult_TEXT_TOO_LONG,                    /**< Request failure because the text provided is longer than 512 characters */
    iBPResult_BITMAP_CONVERSION_ERROR,          /**< Request failure if the provided bitmap can not be converted to monochrome configuration */
    iBPResult_WRONG_LOGO_NAME_LENGTH,           /**< The logo name passed as argument is inappropriate (number of characters should be in the range [4, 8]) */
    iBPResult_PRINTING_ERROR,                   /**< Printer Error */
    iBPResult_PAPER_OUT,                        /**< No more paper in the printer */
    iBPResult_PRINTER_LOW_BATT                  /**< Printer is in low battery condition */
};

/*!
 Type Redefinition of @ref eiBPResult Enumeration.
 */
typedef enum eiBPResult iBPResult;


/*!
 @anchor eiBPFont
 An enumeration of the font supported on Telium side
 */
enum eiBPFont {
    ISO8859_1,                     /**< Latin-1 Western European */
    ISO8859_2,                     /**< Latin-2 Central European */
    ISO8859_3,                     /**< Latin-3 South European */
    ISO8859_5,                     /**< Latin/Cyrillic. Covers mostly Slavic languages that use a Cyrillic alphabet, including Belarusian, Bulgarian, Macedonian, Russian, Serbian, and Ukrainian (partial). */
    ISO8859_6,                     /**< Latin/Arabic. Covers the most common Arabic language characters. */
    ISO8859_7,                     /**< Latin/Greek. Covers the modern Greek language (monotonic orthography). Can also be used for Ancient Greek written without accents or in monotonic orthography, but lacks the diacritics for polytonic orthography. */
    ISO8859_15                     /**< A revision of 8859-1 that removes some little-used symbols, replacing them with the euro sign € and the letters Š, š, Ž, ž, Œ, œ, and Ÿ, which completes the coverage of French, Finnish and Estonian. */
};


/*!
    Type Redefinition of @ref eiBPFont Enumeration.
*/
typedef enum eiBPFont iBPFont;



/*!
    @brief Bluetooth Printer API extension of the PCL library
   <p>
      This category extends the API of the ICAdministration class provided by the PCL library. The functions defined in this file are used to communicate with the bluetooth printer driver of the Companion to print certain types of documents composed of text and bitmaps.
   </p>
*/
@interface ICAdministration (iBP)

/*!
 @anchor     font
 @brief      The font selected to print text
 */
@property (copy, nonatomic) NSString * font;

/*!
    @brief Tries to open a session to the Companion bluetooth printer
    @anchor iBPOpenPrinter
    <p>
        This call must succeed to be able to send printing commands to the Companion's bluetooth printer. If a session is already opened, this method returns immediately success as a result.<br />
        This call is blocking and has a 15 seconds timeout, after which it is deemed to have failed.<br />
        When the printing is done, the session should be closed by calling @ref iBPClosePrinter.
    </p>
    @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
*/
-(iBPResult)iBPOpenPrinter;


/*!
    @brief Tries to close an already opened printing session
    @anchor iBPClosePrinter
    <p>
        When no session has already been opened to the Companion's bluetooth printer, this method returns imediately with a success, otherwise it inquires the Companion printer to close the existant one.<br />
        If this command succeeds, all subsequent printing commands will fail if no new session is created by calling @ref iBPOpenPrinter.<br />
                This call is blocking and has a 15 seconds timeout.
    </p>
    @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
*/
-(iBPResult)iBPClosePrinter;

/**
    @anchor     iBPPrintText
    @brief      Request to print the text provided as parameter
    <p>
        The length of the string to be printed should not exceed 512 characters otherwise the call will fail.<br />
        This call is blocking and has a timeout of 15 seconds. Before print text you should choose the font using @ref iBPSetFont. If @ref iBPSetFont is not used the default font is ISO8859-15.
    </p>
    @param      text NSString object of the text to be printed. The  length of this string must be 512 characters at most.
    @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
*/
-(iBPResult)iBPPrintText:(NSString *)text;


/*!
    @anchor  iBPPrintBitmap
    @brief   Request the print of the bitmap  provided as parameter
    <p align="justify">
        This method accepts all image types that are supported by iOS. The UIImage object undergoes is converted to an indexed black and white monochrome bitmap where each pixel is encoded on a bit of data. When printed, the bitmap keeps its original size if it does not exceed the maximum dimensions, otherwise it is scaled to fit within those.<br />
        A printing session must already be opened before calling this method, otherwise the call returns immediately with a failure result.<br />
        This call is blocking and has a timeout of 30 seconds.<br />
        See @ref iBPPrintBitmapSizeAlignment for more drawing options.
    </p>
    @param      image UIImage object to be printed.
    @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
*/
-(iBPResult)iBPPrintBitmap:(UIImage *)image;

/*!
 @anchor  iBPPrintBitmapLastBitmap
 @brief   Request the print of the bitmap  provided as parameter
 <p align="justify">
 This method is extended from @ref iBPPrintBitmap and added to fix a print issue that occurs when using a CUSTOM BT printer. More generally you should use this method if the bitmap to be printed is higher than 1024 or if you want to print several bitmaps in a consecutive way.  This method accepts all image types that are supported by iOS. The UIImage object undergoes is converted to an indexed black and white monochrome bitmap where each pixel is encoded on a bit of data. When printed, the bitmap keeps its original size if it does not exceed the maximum dimensions, otherwise it is scaled to fit within those.<br />
 A printing session must already be opened before calling this method, otherwise the call returns immediately with a failure result.<br />
 This call is blocking and has a timeout of 30 seconds.<br />
 See @ref iBPPrintBitmapSizeAlignment for more drawing options.
 </p>
 @param      image UIImage object to be printed.
 @param      isLastBitmap BOOL specify if the bitmap to print is the last.
 @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
 */
-(iBPResult)iBPPrintBitmap:(UIImage *)image lastBitmap:(BOOL)isLastBitmap;

/*!
    @anchor  iBPPrintBitmapSizeAlignment
    @brief   Print a bitmap and scale it according to the provided CGSize parameter
    <p align="justify">
        This methods works exactly the same as @ref iBPPrintBitmap (It is synchronous and has a 15 seconds timeout), except that the bitmap can be scaled using the bitmapSize argument and can be aligned horizontally (left, center or right alignment). The size of the bitmap can be decreased or increased based on the bitmapSize parameter, and is always limited by the maximum dimensions that a bitmap may have to be printed.<br />
            In order to draw the bitmap in its original size, the application may provide a CGSize structure initialized with the bitmap's width and height.
    </p>
    @param      image UIImage object to be printed.
    @param      bitmapSize  A CGSize structure that specifies the preferred bitmap size.
    @param      alignment   The alignment of the bitmap within the printed ticket (left, right or center). This may be one of the constants of *NSTextAlignment*
    @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
*/
-(iBPResult)iBPPrintBitmap:(UIImage *)image size:(CGSize)bitmapSize alignment:(NSTextAlignment)alignment;

/*!
 @anchor  iBPPrintBitmapSizeAlignmentLastBitmap
 @brief   Print a bitmap and scale it according to the provided CGSize parameter
 <p align="justify">
    This method is extended from @ref iBPPrintBitmapSizeAlignment and added to fix a print issue that occurs when using a CUSTOM BT printer. More generally you should use this method if the bitmap to be printed is higher than 1024 or if you want to print several bitmaps in a consecutive way.This methods works exactly the same as @ref iBPPrintBitmapSizeAlignment (It is synchronous and has a 15 seconds timeout), except that the bitmap can be scaled using the bitmapSize argument and can be aligned horizontally (left, center or right alignment). The size of the bitmap can be decreased or increased based on the bitmapSize parameter, and is always limited by the maximum dimensions that a bitmap may have to be printed.<br />
 In order to draw the bitmap in its original size, the application may provide a CGSize structure initialized with the bitmap's width and height.
 </p>
 @param      image UIImage object to be printed.
 @param      bitmapSize  A CGSize structure that specifies the preferred bitmap size.
 @param      alignment   The alignment of the bitmap within the printed ticket (left, right or center). This may be one of the constants of *NSTextAlignment*.
 @param      isLastBitmap BOOL specify if the bitmap to print is the last.
 @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
 */
-(iBPResult)iBPPrintBitmap:(UIImage *)image size:(CGSize)bitmapSize alignment:(NSTextAlignment)alignment lastBitmap:(BOOL)isLastBitmap;

/*!
    @anchor     iBPStoreLogo
    @brief      Stores logos to the Companion.
    <p>Logos are bitmaps identified by a name composed of 8 characters at most. Logos are also converted to monochrome black and white bitmaps and undergo the same transaformations as for bitmaps printed using @ref iBPPrintBitmap.<br />
       This call is blocking and has a timeout of 30 seconds.
    </p>
    @param      name  The name of the logo (Composed of 8 characters at most)
    @param      logo UIImage object of the logo to be loaded into the terminal.
    @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
*/
-(iBPResult)iBPStoreLogoWithName:(NSString *)name andImage:(UIImage *)logo;


/*!
    @anchor     iBPPrintLogo
    @brief      Request the printing of a logo stored inside the Companion.
    <p>
        Logos are bitmaps identified by a name and store inside the Companion either manually (by using the file transfer tool LLT) or programmatically by calling the @ref iBPStoreLogo method. To print a logo, we just refer to the logo's name and provide as an argument to @ref iBPPrintLogo.
        This call is blocking and has a timeout of 20 seconds.
    </p>
    @param      name The name of the logo.
    @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
*/
-(iBPResult)iBPPrintLogoWithName:(NSString *)name;


/*!
    @anchor     iBPGetPrinterStatus
    @brief      Gets the printer's status
    <p>
        This call is blocking and has a timeout of 10 seconds.
    </p>
    @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
*/
-(iBPResult)iBPGetPrinterStatus;

/**
 @anchor     iBPSetFont
 @brief      Request to set the font provided as parameter
 <p>
 This call permits to select the font used to print text using @ref iBPPrintText.
 </p>
 @param      selectedFontToTelium the encoding format ISO8859 supported by Telium @ref eiBPFont.
 @result     One of the enumerations of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
 */
-(iBPResult)iBPSetFont:(iBPFont *) selectedFontToTelium;

/**
 @anchor    iBPOpenCashDrawer
 @brief     Request to open the cash drawer connected to a printer
 <p>
 This call allow you to open the cash drawer connected to a printer.
 </p>
 @result    One of the enumeration of @ref eiBPResult. It is iBPResult_OK when the call succeeds.
 */
-(iBPResult)iBPOpenCashDrawer;

/*!
 @anchor     iBPisBatteryLow
 @brief      Gets if the printer is in battery low mode
 <p>
 This call is blocking and has a timeout of 10 seconds.
 </p>
 @result     TRUE if the printer is in low battery, FALSE in other case.
 */
-(BOOL)iBPisBatteryLow;
/*!
 @anchor     iBPisConnected
 @brief      Gets if the printer is connected
 <p>
 This call is blocking and has a timeout of 10 seconds.
 </p>
 @result     TRUE if the printer is connected, FALSE in other case.
 */
-(BOOL)iBPisConnected;

/*!
    @anchor     iBPMaxBitmapWidth
    @brief      Gets the maximum authorized width of a bitmap to be printed
    @result     Unsigned integer value of the maximum authorized width for a bitmap
*/
@property (nonatomic, readonly, getter = iBPMaxBitmapWidth) NSUInteger iBPMaxBitmapWidth;

/*!
    @anchor     iBPMaxBitmapHeight
    @brief      Gets the maximum authorized height of a bitmap to be printed
    @result     Unsigned integer value of the maximum authorized height for a bitmap
*/
@property (nonatomic, readonly, getter = iBPMaxBitmapHeight) NSUInteger iBPMaxBitmapHeight;


@end
/*!
@}
*/
