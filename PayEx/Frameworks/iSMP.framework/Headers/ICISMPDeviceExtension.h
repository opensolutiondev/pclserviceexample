//
//  ICISMPDeviceExtension.h
//  PCL Library
//
//  Created by Boris LECLERE on 7/18/12.
//  Copyright (c) 2012 Ingenico. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICISMPDevice.h"

/*!
    @file	 ICISMPDeviceExtension.h
    @brief   Header file that contains the definitions of @ref ICISMPDeviceExtension class and its associated protocol @ref ICISMPDeviceExtensionDelegate protocol
*/



@protocol ICISMPDeviceExtensionDelegate;



/*!
    @anchor		ICISMPDeviceExtension
    @brief      This class extends @ref ICISMPDevice by adding interface methods to send and receive data.
    <div align="justify">
        This class has not to be used directly by applications since it does not open a Companion session itself. It provides an API for sending and receiving data to the classes that implement a raw communication channel,like @ref ICTransaction and @ref ICSPP. These two classes inherit from @ref ICISMPDeviceExtension and can therefore call its methods to send/receive data.<br /><br />
    </div>
*/
@interface ICISMPDeviceExtension : ICISMPDevice
{
	NSMutableArray * m_SendList;        /**< List to send */
}

/*!
    @anchor     deviceExtensionDelegate
    @brief      The delegate of an @ref ICISMPDeviceExtension object
    <p>This property conforms to the @ref ICISMPDeviceDelegate and @ref ICISMPDeviceExtensionDelegate protocols, which means that the assigned object will receive the events of @ref ICISMPDevice and @ref ICISMPDeviceExtension classes.</p>
*/
@property(nonatomic, weak) id<ICISMPDeviceDelegate, ICISMPDeviceExtensionDelegate> delegate;


/*!
    @anchor     SendData
    @brief      Send a data buffer synchronously
    <p>
        This method writes the NSData object provided in argument on the output stream of the SPP channel, and returns the number of bytes that have been written. This method is to be used in a context where the data is sent synchronously, like in a loop for example. This means that the application should implement a loop an continue calling @ref SendData until all the data is written to the terminal.
    </p>
    @param      Data A NSData object containing the data bytes to be written to the terminal
    @result		The number of bytes that have been written, -1 if an error occured
*/
-(int)SendData:(NSData *)Data;


/*!
    @anchor     SendDataAsync
    @brief      Send a data buffer asynchronously
    <p>
        This method writes the NSData object provided in argument on the output stream of the SPP channel. It returns immediately whatever the length of the data is. The data buffer is provided to the @ref ICSPP class which sends it asynchronously.
    </p>
    @param      Data A NSData object containing the data bytes to be written to the terminal
    @result		YES if the NSData object is valid and the SPP channel is available, otherwise NO.
*/
-(bool)SendDataAsync:(NSData *)Data;

/*!
    @anchor     SendString
    @brief      Send a text string synchronously on the SPP channel
    <p>
        This method writes the NSString object provided in argument on the output stream of the SPP channel. The text is written asynchronously like for @ref SendData method, which means that the application should check if the returned length is equal to the length of the string, otherwise, other calls to SendString would be necessary to send the remaining part.
    </p>
    @param      String A NSString object containing the data bytes to be written to the terminal
    @result		The number of bytes that have been written
*/
-(int)SendString:(NSString *)String;


/*!
    @anchor     SendStringAsync
    @brief      Send a text string asynchronously on the SPP channel
    <p>
        This method asynchronously writes the NSString object provided in argument on the output stream of the SPP channel. The NSString object is written in one time and the application has not to call this method more than once.
    </p>
    @param      String A NSString object containing the data bytes to be written to the terminal
    @result		YES if the NSString object is valid and the SPP channel is available, otherwise NO.
*/
-(bool)SendStringAsync:(NSString *)String;


// Reception
@property(getter = TotalNbFrameReceived) unsigned int m_TotalNbFrameReceived;                               /**< Number of frame received */

// Reception Property
@property(getter = ReceiveBufferSize, setter = SetReceiveBufferSize:) unsigned int m_ReceiveBufferSize;     /**< Size of received buffer */

// Sent
@property(getter = TotalNbFrameSent) unsigned int m_TotalNbFrameSent;                                       /**< Number of frame sent */


@end




/*!
    @anchor     ICISMPDeviceExtensionDelegate
    @brief      The ICISMPDeviceExtension's delegate methods
    <p>
        The @ref ICISMPDeviceExtensionDelegate protocol defines the events of an @ref ICISMPDeviceExtension object. Those methods declared within this protocol, depending on whether they are required or optional, may be implemented by any object that conforms to @ref ICISMPDeviceExtensionDelegate.
    </p>
*/
@protocol ICISMPDeviceExtensionDelegate <ICISMPDeviceDelegate>

@optional

/*!
    @anchor     didReceiveDatafromICISMPDevice
    @brief      Function called when the @ref ICISMPDeviceExtension object receives some data
    <p>
        This callback method is required and must be implemented by the delegate of an @ref ICISMPDeviceExtension object, since it is the only way of retrieving the received data.
    </p>
    @param      Data NSData object containing the received data
    @param      Sender The sender of the event.
*/
-(void)didReceiveData:(NSData *)Data fromICISMPDevice:(ICISMPDevice *)Sender;


/*!
    @anchor     willReceiveData
    @brief      Function called when a @ref ICISMPDeviceExtension object starts receiving some data
    <p>
        This function is optional. It is called just before @ref didReceiveDatafromICISMPDevice to inform that the sender object started receiving some data, but that the latter is not yet ready to be provided to the delegate.
    </p>
    @param      Sender The sender of the event.
*/
-(void)willReceiveData:(ICISMPDevice *)Sender;


/*!
    @anchor     willSendData
    @brief      Function called when a @ref ICISMPDeviceExtension object is about to send some data
    <p>
        This function is optional. It is called after calling one of the methods used to send data or text. It informs the delegate that the data it provided to an @ref ICISMPDeviceExtension object is about to be sent.
    </p>
    @param      Sender The sender of the event.
*/
-(void)willSendData:(ICISMPDevice *)Sender;


/*!
    @anchor     didSendDatawithNumberOfBytesSentfromICISMPDevice
    @brief      Function called when a @ref ICISMPDeviceExtension object successfully sent a chunk of data
    <p>
        This function is optional. It is called each time a data buffer is sent on the @ref ICISMPDeviceExtension output stream.
    </p>
    @param      Data    A NSData buffer of the data that is written on the output stream
    @param      NbBytesSent The length of the data that is written
    @param      Sender The sender of the event.
*/
-(void)didSendData:(NSData *)Data withNumberOfBytesSent:(unsigned int) NbBytesSent fromICISMPDevice:(ICISMPDevice *)Sender;

@end
