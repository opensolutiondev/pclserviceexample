//
//  ICPrinter.h
//  PCL Library
//
//  Created by Christophe Fontaine on 22/06/10.
//  Copyright 2010 Ingenico. All rights reserved.
//

/*!
    @file       ICPrinter.h
    @brief      Header file for the ICPrinter class and its related data structure and protocol definitions
*/


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ICISMPDevice.h"


/*!
    @defgroup   ICPrinterGroup   Native Printing
    @brief      All classes and protocols provided by the PCL library to emulate a printer
    @{
*/

@protocol ICPrinterDelegate;


/*!
    @brief       Printer emulation
    <p>This class provides native printing support for the terminal applications</p>
*/
@interface ICPrinter : ICISMPDevice {
	
	NSUInteger			  _microlineCount;                      /**< The number of received microlines during a print session */
	
	NSUInteger			  _microlineNumber;                     /**< The microline number sent by the Companion (contains the microline number in the first 3 bytes, and the printing session in the last byte) */
}


/*!
    @defgroup   ICPrinterInitialization ICPrinter Initialization
    @ingroup    ICPrinterGroup
    @brief      Steps in order to initialize an @ref ICPrinter object
    <p>To get the printer channel ready, do as following:
        <ul>
            <li>Point to the shared instance by calling @ref sharedPrinter</li>
            <li>Set the @ref printerDelegate property to and object that implements the @ref ICISMPDeviceDelegate and @ref ICPrinterDelegate protocols.</li>
            <li>Check the @ref isAvailable property to see if the connection has been properly established to the Companion and the printer channel opened</li>
        </ul>
    </p>
    @{
*/

/*!
    @anchor     sharePrinter
	@brief      Returns the unique shared instance of @ref ICPrinter
    <p>This method returns an autoreleased object.</p>
	@result		A pointer to the shared instance
*/
+(id)sharedPrinter;



/*!
    @anchor     printerDelegate
	@brief      The delegate object that should implement the @ref ICPrinterDelegate protocol
    <p>By implementing the protocol, the delegate is notified with @ref ICPrinter's events</p>
*/
@property(nonatomic, weak) id<ICISMPDeviceDelegate,ICPrinterDelegate> delegate;

/*! 
    @}
*/


@end





#pragma mark ICPrinterDelegate protocol


/*!
	@anchor      ICPrinterDelegate
	@brief       The @ref ICPrinter's delegate methods
    <p>These methods should be implemented by a delegate of @ref ICPrinter in order to be notified of its events</p>
*/
@protocol ICPrinterDelegate

@optional


/*!
    @defgroup   ICPrinterCallbacks  ICPrinter Callbacks
    @ingroup    ICPrinterGroup
    @brief      All Printing Notifications
    <p>Callbacks defined within @ref ICPrinterDelegate protocol than can be implemented by the delegate of @ref ICPrinter.</p>
    @{
*/

/*!
    @anchor     receivedPrinterData
	@brief      This method is called every time the printer channel receives data from the Companion
    <p align="justify">
        All the data passed through calls to @ref receivedPrinterData must be buffered until a @ref printingDidEndWithRowNumber call is performed. The receiver should then generate the receipt using the data it gathered so far.
    </p>
	@param		 data A NSData containing a set of line dots to be printed.
*/
-(void)receivedPrinterData:(NSData *)data;




/*!
    @brief      This method is called every time the printer channel receives data from the Companion
    <p>
        The application should bufferize the data and print it sequencially. This callback provides the number of received lines since it is meant to be used when the end of printing is not known in advance.<br />
        Please note that this callback should be used separately of the @ref printingDidEndWithRowNumber: callback (must not be implementing if you use this one).</p>
    @param		 data A NSData containing a set of line dots to be printed.
    @param       count The number of printed lines contained within data
*/
-(void)receivedPrinterData:(NSData *)data numberOfLines:(NSInteger)count;


/*!
	@anchor		printingDidEndWithRowNumber
	@brief      This method is triggered when the ICPrinter object has received all the data needed for printing a receipt
    <p>
        Each pixel of the receipt is encoded using one bit of data, and the number of rows is given by the count parameter. The width of the graphics to be rendred is then equal to 8 times the size of the received data divided by the number of rows.
        Please note that this callback should be used separately of the @ref receivedPrinterData: callback (must not be implementing if you use this one).
    </p>
	@param		 count The number of rows of the receipt to be printed
*/
-(void)printingDidEndWithRowNumber:(NSUInteger)count;

/*! 
    @}
*/


@end

/*! 
    @}
*/

#pragma mark -
