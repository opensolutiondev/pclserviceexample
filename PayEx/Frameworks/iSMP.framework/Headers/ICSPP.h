//
//  ICSPP.h
//  PCL Library
//
//  Created by Hichem Boussetta on 24/05/12.
//  Copyright (c) 2012 Ingenico. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICISMPDeviceExtension.h"

/*!
    @file	 ICSPP.h
    @brief   Header file of the ICSPP class
*/



/*!
    @anchor		ICSPP
    @brief      Transparent SPP Channel Class

        This class provides a transparent SPP (Bluetooth Serial Port Profile) channel for iOS application that require to communicate with a Bluetooth device paired with the Companion.<br />
        This class inherits from @ref ICISMPDeviceExtension that provides all the required methods to send data or text synchronously or asynchronously. Those include:</div>
        <ul align="justify">
            <li>@ref SendData</li>
            <li>@ref SendDataAsync</li>
            <li>@ref SendString</li>
            <li>@ref SendStringAsync</li>
        </ul>
        
        The data that the ICSPP object can be retrieved through the @ref ICSPPDelegate property that conforms to the @ref ICISMPDeviceExtensionDelegate protocol that provides the @ref didReceiveDatafromICISMPDevice method for this purpose.<br /><br />
        
        The initialization of the ICSPP channel can be achieved by following these steps:
        <ul align="justify">
            <li>Get the shared instance of ICSPP by calling @ref sharedChannel - The returned object is autoreleased and needs to be retained</li>
            <li>Assign the @ref ICSPPDelegate property to the object that will receive the events of ICSPP</li>
            <li>Check the @ref isAvailable property to make sure the channel is ready - If the returned value is YES, the object is ready to send and receive data/text</li>
        </ul>
*/
@interface ICSPP : ICISMPDeviceExtension


/*!
    @brief      Returns the unique shared instance of ICSPP
    <p>The object returned by this method is autoreleased and must be retained to stay alive.</p>
    @result		A pointer to the shared instance
*/
+(ICSPP *)sharedChannel;


/*!
    @anchor     ICSPPDelegate
    @brief      The delegate of an @ref ICSPP object
    <p>This property should be assigned the reference of a delegate object that will receive the events of @ref ICSPP - This object should implement the @ref ICISMPDeviceDelegate and @ref ICISMPDeviceExtensionDelegate protocols</p>
*/
@property (nonatomic, weak) id<ICISMPDeviceDelegate, ICISMPDeviceExtensionDelegate> delegate;


@end
