/*
 *  revision.h
 *  EasyPayEMVCradle
 *
 *  Created by Christophe Fontaine on 22/10/10.
 *  Copyright 2010 Ingenico. All rights reserved.
 *
 */

#ifndef __REVISION_H_
#define __REVISION_H_

// ICISPMGitVersion : 05745052329e96d4cf214cbd308360647f8cb618
// ICISPMVersion    : iSMP v5.50

extern NSString * ICISMPGitVersion;
extern NSString * ICISMPVersion;

extern NSString * ICISMPSvnVersion __attribute__((deprecated));

extern const unsigned char iSMPVersionString[];
extern const double iSMPVersionNumber;
#endif // __REVISION_H_

