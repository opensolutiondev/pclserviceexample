//
//  ICAdministration.h
//  PCL Library
//
//  Created by Hichem Boussetta on 19/07/10.
//  Copyright 2010 Ingenico. All rights reserved.
//


/*!
    @file       ICAdministration.h
    @brief      Header file for ICAdministration class and its related data structures and protocol definitions
*/


#import <Foundation/Foundation.h>
#import "ICISMPDevice.h"

@class ICTmsInformation;
@class ICSoftwareComponent;

#define PROFILE_NAME_SIZE   11                          /**< The tms  SSL profile name max size is 11 */
#define CONTRAT_NAME_SIZE   10                          /**< The tms identifier name max size is 10 */
#define NB_PROFILE          20                          /**< The tms profile max number is 20 */
#define PORT_MAX            65535                       /**< The tms port can't exceed 65535 */
#define HOSTNAME_NAME_SIZE  257                         /**< The tms hostname lenght can't exceed 257 caracters */

/** @brief      The device's information */
typedef struct _ICDeviceInformation {
	NSInteger		serialNumber;                       /**< The device's truncated serial number (8 last digits) */
	NSInteger		reference;                          /**< The device's part number */
	char			protocol[20] NS_REFINED_FOR_SWIFT;  /**< The payment protocol used by the device */
} ICDeviceInformation;

/** @brief returns `protocol` as an NSString. Mapped in Swift to ICDeviceInformation.protocolString */
NSString *ICDeviceInformationGetProtocolString(const ICDeviceInformation *info) NS_SWIFT_NAME(getter:ICDeviceInformation.protocol(self:));


/*!
	@anchor		eICDeviceKeys
	@brief      Terminal Keyboard Constants
*/
enum eICDeviceKeys {
	ICNum0			= '0',                              /**< Numeric Key 0 */
	ICNum1			= '1',                              /**< Numeric Key 1 */
	ICNum2			= '2',                              /**< Numeric Key 2 */
	ICNum3			= '3',                              /**< Numeric Key 3 */
	ICNum4			= '4',                              /**< Numeric Key 4 */
	ICNum5			= '5',                              /**< Numeric Key 5 */
	ICNum6			= '6',                              /**< Numeric Key 6 */
	ICNum7			= '7',                              /**< Numeric Key 7 */
	ICNum8			= '8',                              /**< Numeric Key 8 */
	ICNum9			= '9',                              /**< Numeric Key 9 */
	ICKeyDot		= '.',                              /**< Dot Key */
	ICKeyPaperFeed	= 0x07,                             /**< Paper Feed Key */
	ICKeyGreen		= 0x16,                             /**< Green Key */
	ICKeyRed		= 0x17,                             /**< Red Key */
	ICKeyYellow		= 0x18,                             /**< Yellow Key */
	ICKeyF1			= 0x19,                             /**< F1 Key */
	ICKeyF2			= 0x20,                             /**< F2 Key */
	ICKeyF3			= 0x21,                             /**< F3 Key */
	ICKeyF4			= 0x22,                             /**< F4 Key */
	ICKeyUp			= 0x23,                             /**< UP Key */
	ICKeyDown		= 0x24,                             /**< Down Key */
	ICKeyOK			= 0x25,                             /**< OK Key */
	ICKeyC			= 0x26,                             /**< C Key */
	ICKeyF			= 0x28,                             /**< F Key */
};

/*!
	@anchor		eICDeviceSoftwareComponentType
	@brief      The terminal's software component types
*/
enum eICDeviceSoftwareComponentType {
	ICDeviceApplication = 0,                            /**< Application */
	ICDeviceLibrary,                                    /**< Library */
	ICDeviceDriver,                                     /**< Driver */
	ICDeviceParameter                                   /**< Parameter File */
};


/*!
    @anchor		iSMPPeripheral
    @brief      Enumeration of all peripheral with Companion
*/
typedef enum {
    SPP_Apple                                           /**< SPP Apple */
} iSMPPeripheral;

#pragma mark Transaction Constants

#pragma mark ICTransactionAccountType
/** @anchor ICTransactionAccountType
 @brief The enumeration of transaction account types

 | Cases                    | Documentation     |
 | ------------------------ | ----------------- |
 | All                      | All Account Types |
 | Bancaire                 | Bancaire          |
 | AmericanExpress          | American Express  |
 | Aurore                   | Aurore            |
 | Cetelem                  | Cetelem           |
 | Cofinoga                 | Cofinoga          |
 | DinerClub                | Diner Club        |
 | Pass                     | Pass              |
 | Franfinance              | Franfinance       |
 | JCB                      | JCB               |
 | Accord                   | Accord            |
 | Cheque                   | Cheque            |
 | Finaref                  | Finaref           |
 | Modeus                   | Modeus            |
 | Moneo                    | Moneo             |
 | PinaultPrintempsRedoute  | Pinault Printemps Redoute |
 | Mondex                   | Mondex            |
 */
typedef NS_ENUM(unsigned char, ICTransactionAccountType) {
    ICTransactionAccountTypeAll     				= '0',
    ICTransactionAccountTypeBancaire				= '1',
    ICTransactionAccountTypeAmericanExpress			= '2',
    ICTransactionAccountTypeAurore					= '3',
    ICTransactionAccountTypeCetelem					= '4',
    ICTransactionAccountTypeCofinoga				= '5',
    ICTransactionAccountTypeDinerClub				= '6',
    ICTransactionAccountTypePass					= '7',
    ICTransactionAccountTypeFranfinance				= '8',
    ICTransactionAccountTypeJCB						= '9',
    ICTransactionAccountTypeAccord					= 'A',
    ICTransactionAccountTypeCheque					= 'C',
    ICTransactionAccountTypeFinaref					= 'F',
    ICTransactionAccountTypeModeus					= 'M',
    ICTransactionAccountTypeMoneo					= 'O',
    ICTransactionAccountTypePinaultPrintempsRedoute = 'P',
    ICTransactionAccountTypeMondex					= 'X',
};

#pragma mark ICTransactionType
/** @anchor ICTransactionType
 @brief The enumeration of transaction types

 | Cases      | Documentation |
 | ---------- | ------------- |
 | Debit      | Debit         |
 | Credit     | Credit        |
 | Annulation | Cancellation  |
 | Duplicata  | Duplicata     |
 | ISO2       | ISO2          |
 | Specific   | Specific      |
 */
typedef NS_ENUM(unsigned char, ICTransactionType) {
    ICTransactionTypeDebit		= '0',
    ICTransactionTypeCredit		= '1',
    ICTransactionTypeAnnulation	= '2',
    ICTransactionTypeDuplicata	= '3',
    ICTransactionTypeISO2		= 'A',
    ICTransactionTypeSpecific	= 'B',
};

#pragma mark ICTransactionAuthorization
/** @anchor ICTransactionAuthorization
 @brief The enumeration of transaction authorization types
 */
typedef NS_ENUM(unsigned char, ICTransactionAuthorization) {
    ICTransactionAuthorization0	= '0',
    ICTransactionAuthorization1	= '1',
    ICTransactionAuthorization2	= '2',
};


/*!
    @defgroup   ICAdministrationGroup Administration
    @brief      All methods, structures and callbacks of ICAdministration class
    @{
*/


@protocol ICAdministrationDelegate;

/*!
    @brief    The Companion's administration channel management class
    <p>
        This class handles the Companion's configuration including power management, device update and other miscellaneous configuration.
    </p>
*/
@interface ICAdministration : ICISMPDevice {
	
	NSMutableArray				* _printerJobs;                             /**< List of in progress printer jobs */
    
	BOOL						  _isWaitingForSignature;                   /**< Boolean value indicating whether ICAdministration is waiting for a signature to be returned by the application*/
	
	NSDictionary				* _fontTable;                               /**< Table of fonts used for printing */
	
	BOOL						_shouldUpdatePowerManagementSettings;       /**< Boolean value indicating whether the power management settings should be updated */
    
    NSUInteger                  _doTransactionTimeout;                      /**< Timeout value used for @ref doTransaction */
    
    NSString                    * _spmciVersion;                            /**< SPMCI component version */
}


/*!
    @defgroup   ICAdministrationInitialization  ICAdministration Initialization
    @ingroup    ICAdministrationGroup
    @brief      Steps in order to initialize the ICAdministration object
    <p>
        All administration routines of the Companion are provided by the @ref ICAdministration class. This can be initialized as following:
        <ul>
            <li>Retain the global class instance by calling @ref sharedICAdministration</li>
            <li>Set the delegate property to a valid object that conforms to the @ref ICISMPDeviceDelegate and @ref ICAdministrationDelegate protocols</li>
            <li>Call the @ref openICAdministration method to open the Administration channel</li>
            <li>Test the @ref isAvailable property to check if the Administration channel is ready to be used</li>
        </ul>
    </p>
    @{
*/


/**
    @anchor     sharedICAdministration
	@brief      Returns the unique shared instance of @ref ICAdministration
    <p>The object returned by this method is autoreleased. It is then necessary to retain it.</p>
	@result		A pointer to the shared instance
*/
+ (instancetype)sharedChannel;

/*!
 @anchor     ICAdministrationDelegate
 @brief      The delegate object. If you use ICPclService, use ICPclService.delegate and not ICAdministartion.delegate.

 This property should be assigned the reference of a delegate object, and it will receive the events declared in ICAdministrationDelegate.
*/
@property (nonatomic, weak) id<ICISMPDeviceDelegate,ICAdministrationDelegate> delegate;

/*!
    @anchor     openICAdministration
    @brief      Open the Administration channel
    <p>This is a synchronous method that opens the Administration channel. It has a timeout of 15 seconds after which the channel is deemed not to be available. This method may return one of the following constants defined within @ref eISMPResult enumeration:
        <ul>
            <li><b>ISMP_Result_SUCCESS</b>: if the call is successful</li>
            <li><b>ISMP_Result_ISMP_NOT_CONNECTED</b>: if the open fails because the terminal is not connected</li>
            <li><b>ISMP_Result_Failure</b>: if the call failed for another reason - this may be because the application does not declare the Administration protocol or that the terminal does not support this protocol.</li>
        </ul>
        The @ref isAvailable property of @ref ICAdministration is set to YES when the call to open is successful, to NO when it fails.
    </p>
    @result		An integer constant defined with the @ref eISMPResult enumeration.
*/
-(iSMPResult)open;

/*!
    @anchor     closeICAdministration
    @brief      Close the Administration channel
        <p>This method closes the @ref ICAdministration channel (its input and output streams) without having to release the object. After this method is called, the @ref isAvailable property is set to NO, and all the subsequent commands that are sent to the terminal fail.
        </p>
*/
-(void)close;


/*!
    @}
*/


#pragma mark Power Management

/*!
    @defgroup   ICAdministrationPowerManagementGroup Companion Power Management Configuration
    @ingroup    ICAdministrationGroup
    @brief      Set of functions to control the power management of the Companion.
    @{
*/


/*!
	@brief      The device's backlight timeout configuration parameter
    <p>This property can be changed by issuing a @ref setTimeouts request</p>
*/
@property (nonatomic, readonly) NSInteger backlightTimeout;

/*!
	@brief      The device's suspend timeout configuration parameter
    <p>This property can be changed by issuing a @ref setTimeouts request</p>
*/
@property (nonatomic, readonly) NSInteger suspendTimeout;

/*!
	@brief      The Companion's battery level
    Retrieve the current battery level of the terminal. This is an integer value between 0 and 100. If an error occurs (Companion not connected for example), this value is equal to -1
*/
@property (nonatomic, readonly) NSInteger batteryLevel;



/*!
	@anchor		setTimeouts
	@brief      Configure the Companion's screen light timeouts
    <p>
        Change the Companion's suspend and backlight timeouts to optimize power consumption. The new values will not necessarily be applied, 
				depending on whether they are within or outside the valid value range supported by the device. In the latter case, the new timeout 
				values are culled to fit the valid range and the @ref setTimeouts request will return NO. The new actual 
				timeouts can then be retrieved from the @ref backlightTimeout and @ref suspendTimeout properties.
    </p>
	@param		backlightTimeout The backlight timeout value. Value between 10 and 1000.
	@param		suspendTimeout The suspend timeout value. Value between 10 and 65535.
	@result		YES if the request was executed by the device, NO in the other case.
*/
-(BOOL)setBacklightTimeout:(NSUInteger)backlightTimeout andSuspendTimeout:(NSUInteger)suspendTimeout;


/*! 
    @}
*/

#pragma mark -
#pragma mark Companion Management


/*!
    @defgroup   ICAdministrationISMPConfig Companion Configuration
    @ingroup    ICAdministrationGroup
    @brief      Companion Configuration Functions
    @{
*/

/*!
    @brief   Set the system date and time of the Companion module
    <p>The setDate request is applied by the Companion only if the difference between the iDevice and the Companion dates is less than 2 days</p>
    @result		YES if the request succeeds, NO otherwise
*/
-(BOOL)setDate;


/*!
    @brief      Get the system date and time of the Companion module
    <p>This method is synchronous and may be time consuming depending on the load of the Companion</p>
    @result		A NSDate containing the current date of the Companion
*/
-(NSDate *)getDate;


/*!
	@brief      Ask the Companion module for its state
    <p>The purpose of this message is to poll the Companion module to get its state, and turn it off in case of inactivity.</p>
	@result		YES if the Companion is idle, NO if it is busy
*/
-(BOOL)isIdle;

/*!
    @brief      Ask the Companion module for the peripheral status
    <p>The purpose of this message is to poll the Companion module to get the status of the peripheral connected on the SPP channel.</p>
    @result		YES if the request succeeds, NO otherwise
    @param		device An enum indicating the link to return status
*/
-(int)getPeripheralStatus:(iSMPPeripheral)device;

/*!
	@brief      Ask the Companion module for its current serial number and reference code
    <p>
        When the <b>getInformation</b> request fails due to a timeout, the integer data members of @ref ICDeviceInformation are set to a negative value.
    </p>
	@result		A ICDeviceInformation structure containing general information about the Companion
*/
-(ICDeviceInformation)getInformation;

/*!
 @anchor     getFullSerialNumber
 @brief      Ask the Companion module for its full serial number
 <p>
 When the <b>getFullSerialNumber</b> request fails due to a timeout, the return is set to "NULL".
 </p>
 @result		A NSString containing the full serial number of the Companion
 */
-(NSString*)getFullSerialNumber;

/*!
    @anchor     reset
	@brief      Reset the terminal.
    <p>
        This request asks the terminal to reboot.<br />
        The call to @ref reset returns immediately and it has no effect when the terminal is disconnected.
    </p>
	@param		resetInfo A constant indicating the reason of the reset
*/
-(void)reset:(NSUInteger)resetInfo;


/*!
	@brief   Simulate a key press on the terminal's keypad
	<p>
        This command simulates a key press event on the terminal's keypad. It returns immediately and has no effect when the terminal is disconnected.
    </p>
	@param		key The key code (refer to @ref eICDeviceKeys for all possible values)
	@result		YES if the command was successful, NO otherwise.
*/
-(BOOL)simulateKey:(NSUInteger)key;


/*!
    @anchor     getSoftwareComponents
	@brief      Get the terminal's software components information
    <p>This method issues a synchronous message to retrieve the software components loaded into the terminal. It returns an array of @ref ICSoftwareComponent objects. Each one of these objects has a name, version, type and CRC code. All binary types are enumerated within @ref eICDeviceSoftwareComponentType</p>
	@result		NSArray containing the components' file names.
*/
-(NSArray<ICSoftwareComponent *> *)getSoftwareComponents;

/*!
@anchor     getSpmciVersion
@brief      Get the terminal's SPMCI component version
<p>This method issues a synchronous message to retrieve the software component version loaded into the terminal. It returns an NSString of @ref ICSoftwareComponent objects.
@result		NSString containing the SPMCI version file.
*/
-(NSString *)getSpmciVersion;

/*!
    @anchor     startRemoteDownload
    @brief      Start a remote download session on the Companion
    <p>
        This method starts the remote download process that updates the software components of the Companion terminal from a remote server. The server's parameters (IP, Port, Logon) have to be configured on the terminal side.<br />
        A network connection is required for the download, and the Companion should be granted access to the network by properly initializing a @ref ICPPP or @ref ICNetwork object.<br />
        This function is blocking and returns as soon as the download is over, or when the 10-minuts timeout is reached. The terminal reboots just after that.
    </p>
    @result		YES if the update is successful, NO if it failed.
*/
-(BOOL)startRemoteDownload;

/*!
    @brief      Update the terminal's encryption key
    @anchor     updateEncryptionKeyWithServerIP
    <p>
        This method is synchronous and takes time to return since it requires downloading an encryption key from a remote server. It should be performed in background to avoid freezing the main thread
    </p>
    @param		ip The IP address of the server from which the key might be downloaded
    @param		port The port to which the key updater should connect
    @result		A return code constant defined within the @ref eISMPResult enumeration. ISMP_Result_SUCCESS is returned if the call succeeds. The error codes are prefixed with <b>ISMP_Result_KEY_INJECTION</b>
*/
-(iSMPResult)updateEncryptionKeyWithServerIP:(NSString *)ip andPort:(NSUInteger)port;


/*!
 @brief      Update the terminal's encryption key by hostname
 @anchor     updateEncryptionKeyWithServerByHostName
 <p>
 This method is synchronous and takes time to return since it requires downloading an encryption key from a remote server. It should be performed in background to avoid freezing the main thread
 </p>
 @param		hostname The hostname address of the server from which the key might be downloaded
 @param		port The port to which the key updater should connect
 @result		A return code constant defined within the @ref eISMPResult enumeration. ISMP_Result_SUCCESS is returned if the call succeeds. The error codes are prefixed with <b>ISMP_Result_KEY_INJECTION</b>
 */
-(iSMPResult)updateEncryptionKeyWithServerByHostName:(NSString *)hostname andPort:(NSUInteger)port;


/*!
    @brief      Check if the encryption key loaded into the terminal is valid
    <p>
        This method checks if an encryption key previously downloaded into the Companion through calling @ref updateEncryptionKeyWithServerIP or @ref updateEncryptionKeyWithServerByHostName is present and valid.
    </p>
    @result		A return code constant defined within the @ref eISMPResult enumeration. ISMP_Result_SUCCESS is returned if the call succeeds.
*/
-(iSMPResult)validateEncryptionKey;


/*!
    @brief      Erases an encryption key loaded into the terminal
    <p>
        This method removes an encryption key previously downloaded into the Companion through calling @ref updateEncryptionKeyWithServerIP or @ref updateEncryptionKeyWithServerByHostName.
    </p>
    @result		A return code constant defined within the @ref eISMPResult enumeration. ISMP_Result_SUCCESS is returned if the call succeeds.
*/
-(iSMPResult)eraseEncryptionKey;


/*!
    @brief   Update the server connection state
    <p>
        This method is used to inform the Companion on whether the iDevice is connected to a remote host. This will affect the icon on the Companion manager, that will display an established or a broken link.
    </p>
    @param		connectionState The connection state to a remote host. Put YES when connected, NO when disconnected.
    @result		YES if the operation succeeds, NO if it fails
*/
-(BOOL)setServerConnectionState:(BOOL)connectionState;


/*!
 @brief   Set the TMS server parameters
 <p>The setTmsInformation request is used to write one or more parameters in the terminal using the @ref ICTmsInformation structure</p>
    @result		A return code constant defined within the @ref eISMPResult enumeration. ISMP_Result_SUCCESS is returned if the call succeeds.
 */
-(iSMPResult)setTmsInformation:(ICTmsInformation*)tmsInfos;


/*!
 @brief      Get the TMS server parameters from the Companion
 <p>This method is synchronous and may be time consuming depending on the load of the Companion</p>
 @result     The parameters set in the terminal are returns in @ref ICTmsInformation structure
 */
-(ICTmsInformation*)getTmsInformation;

/*!
 @brief   Set lock backlight
 <p>The setLockBacklight request is used to manage the backlight of the terminal. Unlock is automic using the keyboard and the lock command is unvalidate.</p>
 @param lockValue 0 to unlock, 3 to lock the backlight.
 @result		A return code constant defined within the @ref eISMPResult enumeration. ISMP_Result_SUCCESS is returned if the call succeeds.
 */
-(iSMPResult)setLockBacklight:(NSUInteger)lockValue;

/*!
 @brief   Send Telium shortcut
 <p>The sendShorcut request is used to access directly to a specific telium Manager menu specified by a shorcut number. The shortcut list is available on the Telium Manager user guide.</p>
 @param shortcutManager the shorcut affected to a specific menu of the manager.
 @result		YES if the operation succeeds, NO if it fails
 */
-(BOOL)sendShortcut:(NSString*)shortcutManager;

/*!
 @brief   Get iOS add-on version
 <p>The getAddonversion request is used to know what is the version of the iOS PCL add-on linked with the application.</p>
 @result        An NSString containing add-on version
 */
-(NSString*)getAddonVersion;

/*!
    @}
*/

#pragma mark -

@end


#pragma mark ICAdministrationDelegate

/*!
    @brief      The Administration Channel's delegate methods
    <p>These methods should be implemented by the @ref ICAdministration's delegate in order to be notified of its events</p>
*/
@protocol ICAdministrationDelegate
@optional

/*!
    @brief      This event is triggered to tell the iOS that it has to awaken the payment application at some point in time
    <p align="justify">
        All the notification data is contained in a UILocalNotification object to be scheduled inside this method. This is done by calling <b>scheduleLocalNotification</b> on the UIApplication shared object. Backward compatibility with iOS 3.x is maintained and the event is not fired in this case. It is also for that reason that the returned notification object is passed as an id.
    </p>
	@param		wakeUpNotification A pointer to a UILocalNotification object that should be registred by the receiving application.
*/
-(void)shouldScheduleWakeUpNotification:(id)wakeUpNotification;




/*!
    @defgroup   ICAdministrationTraceLogging ICAdministration Trace Logging
    @ingroup    ICDeviceTraceLogging
    @{
*/

/*!
	@brief      Log event handler to be implemented by the delegate of the @ref ICAdministration object
    <p>
        The log messages can be filtered based on their severities. These are defined within the @ref SEVERITY_LOG_LEVELS enumeration and can be converted to a human readable form using the @ref severityLevelString and @ref severityLevelStringA methods of the @ref ICISMPDevice class.
    </p>
	@param		 message The log message
	@param		 severity The severity of the log message
*/
-(void)confLogEntry:(NSString*)message withSeverity:(int)severity;


/*!
	@brief       Serial data log event handler to be implemented by the delegate of the @ref ICAdministration object
	@param		 data The serial data incoming or outgoing on the administration channel
	@param		 isIncoming A boolean for the direction in/out of the serial data
*/
-(void)confSerialData:(NSData*)data incoming:(BOOL)isIncoming;

/*!
    @}
*/

@end

/*! 
    @}
*/

#pragma mark -
