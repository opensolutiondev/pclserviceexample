//
//  ICAdministration+StandAlone.h
//  PCL Library
//
//  Created by Christophe Fontaine on 23/02/2011.
//  Copyright 2010 Ingenico. All rights reserved.
//


/*!
    @file       ICAdministration+Standalone.h
    @brief      Header file for ICAdministration's StandAlone category
    <p>
        This file contains the definitions of:
        <ul>
            <li>ICAdministration_StandAlone category</li>
            <li>@ref ICAdministrationStandAloneDelegate holding the callbacks to be implemented when using StandAlone mode functionalities</li>
            <li>Some other structure definitions including do transaction and signature capture requests</li>
        </ul>
    </p>
*/


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ICISMPDevice.h"
#import "ICAdministration.h"

@class ICTransactionRequestObject, ICTransactionReplyObject;

#pragma mark ICAdministration Constants & Structures


/*!
    @defgroup   ICAdministrationTransaction Performing Transactions
    @ingroup    ICAdministrationGroup
    @brief      All methods and structures necessary to perform a transaction on the Companion
    @{
*/

/*!
	@anchor     ICTransactionRequest
	@brief      Transaction request structure in iSMP standalone payment mode
    <p>This is the input data structure for @ref doTransaction. The application should fill all the necessary fields before issuing the transaction request.</p>
*/
typedef struct {
	unsigned short	posNumber;                              /**< The POS number (should be within the range [0 - 255]) */
	char			amount[8];                              /**< The amount for the transaction (left completed with '0'). If the ISO2 track needs to be read, amount should be '0' */
	unsigned char	specificField;                          /**< No longer to be used */
	unsigned char	accountType;                            /**< The kind of payment the POS wishes to use @see @ref ICTransactionAccountType */
	unsigned char	transactionType;                        /**< The type of transaction @see @ref ICTransactionType */
	char			currency[3];                            /**< The currency code in ISO4217 format */
	char			privateData[10];                        /**< Application specific data to be passed to payment application */
	DEPRECATED_ATTRIBUTE
	unsigned char	delay;                                  /**< deprecated and ignored */
	unsigned char	authorization;                          /**< The authorization that the POS asks the Terminal for @see @ref ICTransactionAuthorization */
} ICTransactionRequest;

/*!
	@anchor		ICTransactionReply
	@brief      The output data structure of @ref doTransaction
    <p>
        This structure is the result of the call to @ref doTransaction. It is returned within @ref ICAdministrationStandAloneDelegate callback function @ref transactionDidEnd. The application should analyze the content of the structure in order to know whether the transaction succeeded or failed.
    </p>
*/
typedef struct {
	unsigned short	posNumber;                              /**< The POS number (should be within the range [0 - 255]) */
	unsigned char	operationStatus;                        /**< The status of the payment process */
	char			amount[8];                              /**< The real amount used for the transaction */
	unsigned char	accountType;                            /**< The account type used for the transaction @see @ref ICTransactionAccountType */
	char			currency[3];                            /**< The currency code in ISO4217 format (the same as in the transaction request) */
	char			privateData[10];                        /**< Application specific data to be passed to POS application */
	char			PAN[19];                                /**< No longer to be used */
	char			cardValidity[4];                        /**< No longer to be used */
	char			authorizationNumber[9];                 /**< The authorization number */
	char			CMC7[35];                               /**< No longer to be used*/
	char			ISO2[38];                               /**< No longer to be used*/
	char			FNCI[10];                               /**< No longer to be used */
	char			guarantor[10];                          /**< No longer to be used */
    char            zoneRep[55];                            /**< Response of the cash register connection */
    char            zonePriv[10];                           /**< Private area */
} ICTransactionReply;

/*! 
    @}
*/

/*!
    @anchor		ICSignatureData
    @brief      Signature Data Structure
    <p>This structure is given to the iDevice application within the @ref shouldDoSignatureCapture callback</p>
*/
typedef struct {
	NSUInteger		screenX;                                /**< The X position of the screen */
	NSUInteger		screenY;                                /**< The Y position of the screen */
	NSUInteger		screenWidth;                            /**< The Width of the capture screen */
	NSUInteger		screenHeight;                           /**< The Height of the capture screen */
	NSUInteger		userSignTimeout;                        /**< The timeout for the signature to be captured and sent to the Companion */
} ICSignatureData;


#pragma mark -
@protocol ICAdministrationDelegate;

/*!
    @anchor      ICAdministrationStandAlone
    @brief       The Companion's administration channel management class
    <p>
        This class handles the Companion's configuration including power management, device update and other miscellaneous configuration.
    </p>
*/
@interface ICAdministration (StandAlone)



/*!
    @addtogroup ICAdministrationTransaction
    @{
*/

/*!
 @anchor     doTransactionWithRequest
 @brief      Starts a transaction in standalone payment mode.

 In standalone payment mode, the terminal communicates directly with the bank server without having any intermediate
 application server. The app should setup the @ref ICPPP channel before issueing the transaction in order to ensure the
 terminal has access to the network.

 doTransactionWithRequest is an asynchronous method that returns immediately. As such the result is not returned, but
 given back to the delegate, through the method @ref transactionDidEnd declared in protocol @ref ICPclServiceDelegate.
 The timeout delay is configurable with @ref setDoTransactionTimeout. After that delay
 the transaction is deemed to have failed. Since doTransactionWithRequest is asynchronous, communication can take place
 between the terminal and the application during that time. This makes it possible for example to exchange some messages
 with an application on the terminal or to handle a signature capture request.

 This method is meant to replace both -doTransaction: and -doTransaction:withData:andApplicationNumber:.

 @param      request An @ref ICTransactionRequestObject instance describing the parameters of the transaction.
 */
-(void)doTransactionWithRequest:(ICTransactionRequestObject *)request;

/*!
    @anchor     doTransaction
	@brief      Start a transaction in standalone payment mode.

    This method is called to start a transaction in standalone payment mode. In this mode, the terminal communicates directly with the bank server without having any intermediate application server. The iOS application may act as a gateway by initializing an ICPPP object before calling @ref doTransaction.<br />
    @ref doTransaction is an asynchronous method that returns immediately. The application should conform to @ref ICAdministrationStandAloneDelegate protocol and implement @ref transactionDidEnd to be notified when the transaction is finished. The maximum allowed time for the transaction to be performed is 60 seconds, after which the transaction is deemed to have failed.<br />
    Since @ref doTransaction is asynchronous, it is possible to send other @ref ICAdministration requests to the iOS application before the transaction is over. This makes it possible for example to exchange some messages with a terminal application or to handle a signature capture request.<br />

	@param		request An @ref ICTransactionRequest structure that should be filled with the parameters required to perform the transaction.
*/
-(void)doTransaction:(ICTransactionRequest)request;


/*!
    @anchor     doTransactionExtended
    @brief      Start a transaction in standalone payment mode with additional parameters.
    <p>
        This method acts exactly as @ref doTransaction. The only difference is that it comes with additional parameters to customize the transaction by passing extra data structures and specifying the terminal application that should be invoked. This function is also asynchronous and returns immediately. The result is provided to the iOS application through the callback @ref transactionDidEnd.
    </p>
    @param		request         An @ref ICTransactionRequest structure that should be filled with the parameters required to perform the transaction.
    @param      extendedData    A NSData object that contains any additional transaction data to be passed to the terminal. The maximum data length that can be passed through this parameter is 16 KB.
    @param      appNum          The number of the terminal application that should be invoked to handle the transaction. If this parameter is set to 0, the payment application will be implicitely selected.
*/
-(void)doTransaction:(ICTransactionRequest)request withData:(NSData *)extendedData andApplicationNumber:(NSUInteger)appNum;


/*!
    @brief   Change the transaction timeout
    @param	 timeout The new timeout value in milliseconds
*/
-(void)setDoTransactionTimeout:(NSUInteger)timeout;


/*!   
    @brief      Get the doTransaction timeout
    @result		 The doTransaction timeout value
*/
-(NSUInteger)getDoTransactionTimeout;


/*!
	@anchor		submitSignatureWithImage
	@brief      Submit the signature on the iDevice screen to the Companion
    <p align="justify">
        This method should be called within the @ref shouldDoSignatureCapture event before the signature timeout would expire, otherwise it 
				will be ignored
    </p>
	@param		image A UIImage object containing the signature to be submitted. The image must be in black and white, and have any iOS supported bitmap configuration. More on supported bitmap configurations [here](http://developer.apple.com/library/mac/#qa/qa2001/qa1037.html).
	@result		A boolean value equal to YES if the signature was successfully submitted to the Companion
*/
-(BOOL)submitSignatureWithImage:(UIImage *)image;


/*! 
    @anchor     sendMessage
    @brief      Send a message to the Companion
    <p align="justify">
        The UnderCover messaging makes it possible to wrap any third-party protocol with the protocol used on the administration channel. 
        Before using the UnderCover messaging, the iOS application and the Companion's application must agree on the protocol or the messages that would be exchanged.<br />
        Refer to @ref messageReceivedWithData on how to receive messages from the Companion.
    </p>
    @param		data An NSData object containg the message to be sent. The maximum allowed data length is 1KB.
    @result		YES if the message was received
 */
-(BOOL)sendMessage:(NSData *)data;

/*! 
    @}
*/

@end


#pragma mark ICAdministrationDelegateStandAlone

/*! 
    @anchor      ICAdministrationStandAloneDelegate
    @brief       The Administration Channel's delegate methods
    <p>
        These methods should be implemented by the @ref ICAdministration's delegate in order to be notified of its events
    </p>
*/
@protocol ICAdministrationStandAloneDelegate <NSObject, ICAdministrationDelegate>
@optional

/*!
    @addtogroup ICAdministrationTransaction
    @{
*/

/*!
 @anchor     transactionDidEnd
 @brief      This method is called on the delegate when the terminal is done processing a transaction started through either @ref doTransactionWithRequest, @ref doTransaction or @ref doTransactionEx
 @param      replyReceived    YES if the terminal processed the transaction in time, NO if the timeout is reached.
 @param      transactionReply An ICTransactionReplyObject instance holding all the information about the transaction.
 */
-(void)transactionDidEndWithTimeoutFlag:(BOOL)replyReceived reply:(ICTransactionReplyObject *)transactionReply;

/*!
 @anchor     transactionDidEndResultAndData
 @brief      This method is called on the delegate when the terminal is done processing a transaction started through either @ref doTransactionWithRequest, @ref doTransaction or @ref doTransactionEx
 @param      replyReceived    YES if the Terminal processed the transaction in time, NO if the timeout of 60 seconds is reached.
 @param      transactionReply An ICTransactionReply structure holding all the information about the transaction.
 @param      extendedData     A NSData object containing additional data about the outcome of the transaction. This data is only present and not nil when the transaction was performed by calling @ref doTransactionExtended.
 */
-(void)transactionDidEndWithTimeoutFlag:(BOOL)replyReceived result:(ICTransactionReply)transactionReply andData:(NSData *)extendedData;


/*!
	@anchor		shouldDoSignatureCapture
    @brief      This event is triggered when the iOS receives a Do Signature Capture request
    <p align="justify">
        Upon this event, the application should create a graphics context according to the dimension specified within the @ref ICSignatureData structure. The signature is then drawn on this context and should be submitted before the signature capture timeout is expired.<br />
        A helper class <b>ICSignatureView</b> is provided as a sample. This class derives from UIView and demonstrates how to use the UIView's cocoa touch events to draw a signature. This class may be used directly or replaced in case it does not fulfill the application developer's needs in terms of GUI design.
        </p>
	@param		signatureData A C-structure containing the parameters of the signature capture
*/
-(void)shouldDoSignatureCapture:(ICSignatureData)signatureData;


/*!
    @anchor     signatureTimeoutExceeded
	@brief      This event is triggered if the signature is not captured within the time limits specified within the @ref shouldDoSignatureCapture event's parameters
    <p>
        When this method is called, the application should abort performing the signature capture. Calling @ref submitSignatureWithImage to submit the signature will then have no effect and will be ignored by the PCL library.
    </p>
*/
-(void)signatureTimeoutExceeded;


/*!
 @anchor     messageReceivedWithData
 @brief      This event is triggered when the iDevice receives a message from the Companion
 <p>The iDevice and the Companion applications should agree beforehand on the messages to be exchanged and on how they will be used.</p>
 @param		 data An NSData containing the message
 */
-(void)messageReceivedWithData:(NSData *)data;

/*! 
    @}
*/


/*!
    @defgroup   ICAdministrationPrinting    Printing over the Administration Channel
    @ingroup    ICAdministrationGroup
    @brief      Print receipts composed of text and bitmaps from the Companion on the iDevice applications.
    <p>
        The callbacks that provide the information to be printed are defined within the @ref ICAdministrationStandAloneDelegate protocol. The @ref ICAdministrationDelegate property of the @ref ICAdministration object must be set to a valid object that conforms to the mentioned protocol in order to be notified with the printing callbacks. Those provide the raw data and all the information needed to render the receipt.
    </p>
    @{
*/

/*!
    @anchor     shouldPrintTextWithFontAndAlignment
	@brief      This delegate method is called whenever a text string is available for printing
    <p>
        The font, size and alignment of the text are those used by default by the Companion. It is however possible to override them and use other values.
    </p>
	@param		text A NSString containing the text to be printed
	@param		font An UIFont object including the font and size to be applied to the text when printing
	@param		alignment The alignment of the text
*/
-(void)shouldPrintText:(NSString *)text withFont:(UIFont *)font andAlignment:(NSTextAlignment)alignment;


/*!
    @anchor     shouldPrintTextWithFontAlignmentScaling
    @brief      This delegate method is called whenever a text string is available for printing
    <p>
        This callback is called when formatted text is to be printed. It provides information on how the text should be rendered. This involves the following attributes:
        <ul>
            <li>The text font - The size of the font is not set by the terminal. This means that the iOS application is free to choose the appropriate text size.</li>
            <li>The alignment of the text (left, right or center)</li>
            <li>The X scale ratio (this may be one of the following values: 1, 2 or 4)</li>
            <li>The Y scale ratio (may be one of the values: 1, 2 or 4)</li>
            <li>underlining</li>
        </ul>
        <br />
    </p>
    @param		text A NSString containing the text to be printed
    @param		font An UIFont object including the name of the font that should be used to render the text string
    @param		alignment The alignment of the text
    @param      xFactor The scaling factor to be applied to the text in the X direction
    @param      yFactor The scaling factor to be applied to the text in the Y direction
    @param      underline Specifies whether the text should be underlined or not
*/
-(void)shouldPrintText:(NSString *)text withFont:(UIFont *)font alignment:(NSTextAlignment)alignment XScaling:(NSInteger)xFactor YScaling:(NSInteger)yFactor underline:(BOOL)underline;

/*!
    @anchor     shouldPrintTextWithFontAlignmentScalingUnderlineBold
    @brief      This delegate method is called whenever a text string is available for printing
    <p>
        This callback is called when formatted text is to be printed. It provides information on how the text should be rendered. This involves the following attributes:
        <ul>
            <li>The text font - The size of the font is not set by the terminal. This means that the iOS application is free to choose the appropriate text size.</li>
            <li>The alignment of the text (left, right or center)</li>
            <li>The X scale ratio (this may be one of the following values: 1, 2 or 4)</li>
            <li>The Y scale ratio (may be one of the values: 1, 2 or 4)</li>
            <li>underlining</li>
        </ul>
        <br />
    </p>
    @param		text        A NSString containing the text to be printed
    @param		font        An UIFont object including the name of the font that should be used to render the text string
    @param		alignment   The alignment of the text
    @param      xFactor     The scaling factor to be applied to the text in the X direction
    @param      yFactor     The scaling factor to be applied to the text in the Y direction
    @param      underline   Specifies whether the text should be underlined or not
    @param      bold        Boolean that specifies whether the text should have a bold style
*/
-(void)shouldPrintText:(NSString *)text withFont:(UIFont *)font alignment:(NSTextAlignment)alignment XScaling:(NSInteger)xFactor YScaling:(NSInteger)yFactor underline:(BOOL)underline bold:(BOOL)bold;

/*!
 @anchor     shouldPrintRawTextWithCharsetWithFontAlignmentScalingUnderlineBold
 @brief      This delegate method is called whenever a text string is available for printing
 <p>
 This callback is called when formatted text is to be printed. It provides information on how the text should be rendered. This involves the following attributes:
 <ul>
 <li>The text font - The size of the font is not set by the terminal. This means that the iOS application is free to choose the appropriate text size.</li>
 <li>The text charset - The charset of the text to display. This means that the iOS application shall apply the corresponding charset.</li>
 <li>The alignment of the text (left, right or center)</li>
 <li>The X scale ratio (this may be one of the following values: 1, 2 or 4)</li>
 <li>The Y scale ratio (may be one of the values: 1, 2 or 4)</li>
 <li>underlining</li>
 </ul>
 <br />
 </p>
 @param		text        A char* containing the text to be printed
 @param     charset     The charset used by the text
 @param		font        An UIFont object including the name of the font that should be used to render the text string
 @param		alignment   The alignment of the text
 @param     xFactor     The scaling factor to be applied to the text in the X direction
 @param     yFactor     The scaling factor to be applied to the text in the Y direction
 @param     underline   Specifies whether the text should be underlined or not
 @param     bold        Boolean that specifies whether the text should have a bold style
 */
-(void)shouldPrintRawText:(char *)text withCharset:(NSInteger)charset withFont:(UIFont *)font alignment:(NSTextAlignment)alignment XScaling:(NSInteger)xFactor YScaling:(NSInteger)yFactor underline:(BOOL)underline bold:(BOOL)bold;


/*!
    @anchor      shouldPrintImage
	@brief       This delegate method is called whenever an image is available for printing
	@param		 image An UIImage object containing the image
*/
-(void)shouldPrintImage:(UIImage *)image;

/*!
    @anchor        shouldFeedPaperWithLines
    @brief         This delegate method is called whenever the Companion needs to add empty lines when printing
    @param         lines The number of lines to add
 */
-(void)shouldFeedPaperWithLines:(NSUInteger)lines;

/*!
    @anchor     shouldCutPaper
    @brief      This delegate method is called whenever the Companion needs to cut the paper
*/
-(void)shouldCutPaper;


/*!
 @anchor     shouldStartReceipt
 @brief      This delegate method is called whenever the Companion needs to start printing a receipt
 @param      type The receipt type
 */
-(NSInteger)shouldStartReceipt:(NSInteger)type;

/*!
 @anchor     shouldEndReceipt
 @brief      This delegate method is called whenever the Companion needs to end printing a receipt
 */
-(NSInteger)shouldEndReceipt;

/*!
 @anchor     shouldAddSignature
 @brief      This delegate method is called whenever the Companion needs to add a signature.
 
 */
-(NSInteger)shouldAddSignature;

/*!
 @anchor	shouldSendPclAddonInfos
 @brief      This event is triggered when the iOS receives a PCL informations request from the SPM
 */
-(void)shouldSendPclAddonInfos;

/*!
    @}
*/


@end


#pragma mark -
