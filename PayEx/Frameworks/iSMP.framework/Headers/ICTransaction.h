//
//  ICTransaction.h
//  PCL Library
//
//  Created by Christophe Fontaine on 21/06/10.
//  Copyright 2010 Ingenico. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICISMPDeviceExtension.h"

/*!
    @file	 ICTransaction.h
    @brief   Header file of the ICTransaction class
*/



/*!
	@anchor		ICTransaction
	@brief      Transaction Channel class
    <p>This class provides a raw channel to be used by other third-party ingenico payment libraries.</p>
*/
@interface ICTransaction : ICISMPDeviceExtension {
    DEPRECATED_ATTRIBUTE
	NSMutableSet	*streamObjects;                             /**< Delegates of ICTransaction (not used anymore since the class supports only one delegate now) */
}

/*!
 @anchor     ICTransactionDelegateProperty
 @brief      The delegate of an @ref ICTransaction object
 <p>This property should be assigned the reference of a delegate object that will receive the events of @ref ICTransaction - This object should implement the @ref ICISMPDeviceDelegate and @ref ICISMPDeviceExtensionDelegate protocols</p>
 */
@property (nonatomic, weak) id<ICISMPDeviceDelegate, ICISMPDeviceExtensionDelegate> delegate;

/*!
	@brief      Returns the unique shared instance of ICTransaction
	<p>The object returned by this method is autoreleased and must be retained to stay alive.</p>
	@result		A pointer to the shared instance
*/
+(id) sharedChannel;

/*!
	@brief      Initialise an ICTransaction object with protocolString kICProtoNameSPMTransaction
	@result     The initialized receiver
*/
-(id)init;

/*!
    @anchor     forwardStreamEvents
    @brief      manage delegates of this stream
    <p>
        To be notified of  stream events, implement NSStreamDelegate protocol and subscribe to them by calling @ref forwardStreamEvents.
 */
-(void)forwardStreamEvents:(BOOL)enabled to:(id<NSStreamDelegate>)anObject;

/*!
    @brief  Delegate of ICTransaction class. This property is readonly and to set it properly, @ref forwardStreamEvents must be called.
*/
@property (nonatomic, readonly) id streamEventDelegate;

@end
