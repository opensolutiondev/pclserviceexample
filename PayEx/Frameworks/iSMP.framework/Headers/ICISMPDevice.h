//
//  ICISMPDevice.h
//  PCL Library
//
//  Created by Christophe Fontaine on 21/06/10.
//  Copyright 2010 Ingenico. All rights reserved.
//

/*!
	@file		ICISMPDevice.h
	@brief      Header file for ICISMPDevice class and its related data structures and protocol definitions
*/

#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>

@protocol ICISMPDeviceDelegate;



/*!
    @anchor		eISMPResult
    @brief      Enumeration of all error codes that can be thrown by the PCL library
*/
typedef enum {
    ISMP_Result_Function_Not_Supported = -1,                   /**< The function called is not supported by this type of Terminal */
    ISMP_Result_SUCCESS = 0,                                   /**< The call succeeded */
    ISMP_Result_ISMP_NOT_CONNECTED,                            /**< The call failed because the Companion is not connected */
    ISMP_Result_Failure,                                       /**< The call failed for an unknown reason */
    ISMP_Result_TIMEOUT,                                       /**< The call failed because the timeout was reached. No response was received from the iSMP */
    
    //Key Injection Error Codes
    ISMP_Result_KEY_INJECTION_ABORTED,                         /**< Key Injection Aborted */
    ISMP_Result_KEY_INJECTION_KEY_NOT_FOUND,                   /**< Key Injection failed because no key was found on the server */
    ISMP_Result_KEY_INJECTION_INVALID_HTTP_FILE,               /**< Key Injection failed because the returned HTTP file is invalid */
    ISMP_Result_KEY_INJECTION_INVALID_HTTP_RESPONSE,           /**< Key Injection failed because the returned HTTP response is not 200 OK */
    ISMP_Result_KEY_INJECTION_INVALID_HTTP_HEADER,             /**< Key Injection failed because the returned HTTP header is invalid */
    ISMP_Result_KEY_INJECTION_SSL_NEW_ERROR,                   /**< Key Injection failed because of an SSL initialization failure */
    ISMP_Result_KEY_INJECTION_SSL_CONNECT_ERROR,               /**< Key Injection failed because the connection to the server can not be established */
    ISMP_Result_KEY_INJECTION_SSL_READ_ERROR,                  /**< Key Injection failed because of an SSL reading error */
    ISMP_Result_KEY_INJECTION_SSL_WRITE_ERROR,                 /**< Key Injection failed because of an SSL writing error */
    ISMP_Result_KEY_INJECTION_SSL_PROFILE_ERROR,               /**< Key Injection failed because of an SSL profile error */
    ISMP_Result_KEY_INJECTION_INTERNAL_ERROR,                  /**< Key Injection failed because of an internal error */
    
    ISMP_Result_ENCRYPTION_KEY_NOT_FOUND,                      /**< The encryption key does not exist within the Companion */
    ISMP_Result_ENCRYPTION_KEY_INVALID,                        /**< The encryption key is not valid */
    ISMP_Result_ENCRYPTION_DLL_MISSING                         /**< The encryption DLL is missing within the Companion */
} iSMPResult;



/*!
	@brief       Base class of an Ingenico Cradle device's communiation channel
    <p align="justify">
        The application's developer should not deal directly with this class but with its sub-classes instead, otherwise it throws an exception when initialized (Abstract Class Exception). For instance, the @ref ICAdministration class provides all the necessary functionalities to launch configuration commands and retrieve the parameters of the terminal. The @ref ICBarCodeReader class provides the interface to configure and communicate with the barcode reader embedded into the Companion, the transaction channel works, in the other hand, as a gateway forwarding the payment transactions to the bank servers.<br />
        The @ref ICISMPDevice class provides, however, some attributes about the connection state of the iOS application to the terminal (@ref isAvailable), the name of the protocol used by an ICISMPDevice (@ref protocolName), and gives also access to the input and output streams, which can be handy for debug or for test purposes in case we decide to go without the external accessory protocol and to initialize the device with streams created manually.<br />
        This class was named ICDevice in the library version prior to 3.2, and was renamed, since, to avoid conflicts with another classhaving the same name from Apple's Image Capture Framework.It implements the basic communication login between the iOS device and the Companion and provides some class methods and callbacks that help retrieve information about the library or the Companion. This class can be considered as the core of the PCL library.
    </p>
	@throws      "Abstract Class Exception"
*/
@interface ICISMPDevice : NSObject <NSStreamDelegate> {
	// Companion management
	NSString				* protocolName;                             /**< IAP protocol name that the @ref ICISMPDevice uses for communication with the Companion */
	EASession				* _cradleSession;                           /**< The IAP session opened by the @ref ICISMPDevice to the Companion */
	BOOL					  isAvailable;                              /**< Companion connection state */
	
	NSOutputStream			* outStream;                                /**< Serial output stream */
	NSInputStream			* inStream;                                 /**< Serial input stream */
	
	// iSMP Messages Processing
	NSRecursiveLock			* _inDataLock;                              /**< Used to synchronize access to received data buffer */
	NSMutableData			* _inStreamData;                            /**< Data received from Companion */
	NSMutableDictionary		* _actionLookupTable;                       /**< Map TLV tags to selectors */
	BOOL					  mustProcessReceivedDataOnCurrentThread;   /**< Indicates whether data should be processed in the communication thread (NO by default, messages are processed on the main thread) */
	NSArray					* _spmResponseTags;                         /**< List of all Companion's response Tags on a given channel */
	
	NSOperationQueue		* _requestOperationQueue DEPRECATED_ATTRIBUTE;/**< This operation queue serializes the send operations to the Companion */

	NSCondition				* _waitingForResultCondition;               /**< Condition variable  used by subclassed of @ref ICISMPDevice to synchronize their calls */
	NSMutableArray			* _pendingRequests;                         /**< Array of requests sent but that were not responded to by the terminal */
	
	SEL _processReceivedDataSEL DEPRECATED_ATTRIBUTE;                   /**< Selector called to process received data */
	IMP _processReceivedDataIMP DEPRECATED_ATTRIBUTE;                   /**< Used to optimize the call of _processReceivedDataSEL */
	SEL _simulateEventBytesAvailableforStreamSEL DEPRECATED_ATTRIBUTE;  /**< Selector called to simulate a byte available event */
}


/*!
    @defgroup   ICDeviceISMPConnection  Companion Connection State
    @brief      Check and get notified about connection changes between the iDevice and Companion
    @{
*/

/*!
	@brief      The name of the external accessory protocol implemented by @ref ICISMPDevice
    <p>The protocol names conform to the reverse DNS format.</p>
*/
@property (nonatomic, readonly) NSString    *protocolName;
/*!
	@brief      The state of the @ref ICISMPDevice
    <p>
        This attribute indicates the state of an @ref ICISMPDevice. It is set to NO if the channel corresponding to the protocol name is not open. It is set to YES once the channel is opened.
    </p>
*/
@property (readonly) BOOL isAvailable;


/*!
	@brief      This property is a pointer to the input stream opened by the @ref ICISMPDevice to read data from the Companion
    <p>
        This can be an input stream provided by the Companion session opened by the iOS application to the terminal, or can be an input stream set manually (for testing). In both situations, it can be used to spy the state of the stream.
    </p>
*/
@property (readonly) NSInputStream   *inStream;


/*! 
	@brief      This property is a pointer to the output stream opened by the @ref ICISMPDevice to send data to the Companion
    <p>Like for the input stream, this property provides access to a Companion session's stream or to a stream set manually.</p>
*/
@property (readonly) NSOutputStream  *outStream;

/*!
    @}
*/


/*!
	@brief      The delegate object that should implement the @ref ICISMPDeviceDelegate protocol
    <p>By implementing the protocol, the delegate is notified with @ref ICISMPDevice's events</p>
*/
@property(nonatomic, weak) id<ICISMPDeviceDelegate> delegate;


//------------------------------------------------------------------------------


/*!
	@brief      initialize a new object (the receiver) with a defined protocol string
    <p>An init message is generally coupled with an alloc</p>
	@param      protocolString a string identifying the channel
	@result     The initialized receiver
*/
-(id)initWithProtocolString:(NSString*)protocolString;


/*!
	@brief      initialize a new object (the receiver) with input and output streams
    <p>This method initializes the @ref ICISMPDevice without resorting to the Companion protocol</p>
	@param		 inStream An input stream
	@param		 outStream An output stream
	@result     The initialized receiver.
*/
-(id)initWithStreamIn:(NSInputStream*)inStream 
		 andStreamOut:(NSOutputStream*)outStream DEPRECATED_ATTRIBUTE;



/*!
    @addtogroup   ICDeviceISMPConnection
    @{
*/

// The following methods reads values returned in the authentication process
/*!
    @brief   The connection state to the device
    @result YES if the Companion is connected, no otherwise
*/
+(BOOL) isAvailable;

/*! 
    @}
*/


/*!
    @defgroup   ICDeviceISMPLibraryVersion PCL Library Version
    @brief      Methods to retrieve information about the library version, must not be used (will be deprecated in the future).
    @{
*/

/*!
	@brief      Returns the SVN revision of the library
	@result		The revision string
*/
+(NSString *)getRevisionString;


/*!
	@brief      Returns the public version.
                Returns the official version of the PCL library in the form of “@(#)PROGRAM:iSMP PROJECT:iSMP-x.y”. This is the function to use to check the version of the PCL library. To just get the version number, get the substring of the returned NSString object starting from index 30
	@result		The version string
*/
+(NSString *)getVersionString;

/*! 
    @}
*/


/*!
    @defgroup   ICDeviceISMPInformation Companion Information
    @brief      All software and hardware information about the Companion.
    <p>Those are class methods defined within @ref ICISMPDevice</p>
    @{
*/

/*!
	@brief   The truncated serial number of the device.
	@result returns nil if the device is not connected, the 8 last digits of the serial number otherwise. Use @ref getFullSerialNumber for the full one.
*/
+(NSString*) serialNumber;


/*!
	@brief   The model number of the device
	@result returns nil if the device is not connected, the model number otherwise
*/
+(NSString*) modelNumber;


/*!
	@brief   The firmware revision of the device
	@result returns nil if the device is not connected, the firmware revision otherwise
*/
+(NSString*) firmwareRevision;


/*!
	@brief   The hardware revision of the device
	@result returns nil if the device is not connected, the hardware revision otherwise
*/
+(NSString*) hardwareRevision;


/*!
	@brief   Bluetooth name of the device
	@result returns nil if the device is not connected, the name and serial number separate by a - otherwise (for example IMP352-01234567)
*/
+(NSString*) name;

/*!
 @anchor setWantedDevice
 @brief  Set the device to work with
 @param wantedDevice The device which you want to use
 */
+(void)setWantedDevice:(NSString *)wantedDevice;

/*!
 @anchor getWantedDevice
 @brief   Return the device to work with
 @result returns nil if no device already selected with the @ref setWantedDevice function
 */
+(NSString*) getWantedDevice;

/*!
 @anchor     getConnectedTerminals
 @brief      Get the terminal's connected in Bluetooth
 @result		NSMutableArray * containing the serial number (8 last digits) of each connected Ingenico IAP device.
 */
+(NSMutableArray *)getConnectedTerminals;

/*! 
    @}
*/


/*!
    @defgroup ICDeviceTraceLogging Trace Logging
    @brief All trace routines provided by the PCL library.
    <p>Most of those are provided as callbacks that should be implemented by delegates of the classes that inherit from @ref ICISMPDevice.</p>
    @{
*/


/*!
	@anchor		SEVERITY_LOG_LEVELS
    @brief      Severity Levels
*/
enum SEVERITY_LOG_LEVELS {
	SEV_DEBUG=0,                                        /**< Debug Message */
	SEV_INFO,                                           /**< Information Message */
	SEV_TRACE,                                          /**< Trace Message */
	SEV_WARN,                                           /**< Warning Message */
	SEV_ERROR,                                          /**< Error Message */
	SEV_FATAL,                                          /**< Fatal Error Message */
	SEV_UNKOWN                                          /**< Unknown-Severity Message */
};


/*!
	@anchor		severityLevelString
	@brief      Return a static string corresponding to the severity level
    <p>
        The string returned will have the form 
        <ul>
            <li>"SEV_DEBUG"</li>
            <li>"SEV_INFO "</li>
            <li>"SEV_TRACE"</li>
            <li>"SEV_WARN "</li>
            <li>"SEV_ERROR"</li>
            <li>"SEV_FATAL"</li>
            <li>"SEV_UNKOWN"</li> 
        </ul>
    </p>
*/
+(NSString*) severityLevelString:(int)level;


/*!
	@anchor		severityLevelStringA
	@brief      Return a static char* corresponding to the severity level
*/
+(const char*) severityLevelStringA:(int)level;

/*! 
    @}
*/


@end


#pragma mark -
/*!
	@brief      The @ref ICISMPDevice's delegate methods
    <p>These methods should be implemented by the @ref ICISMPDevice's delegate in order to be notified of its events</p>
*/
@protocol ICISMPDeviceDelegate
@optional


/*!
    @addtogroup     ICDeviceISMPConnection
    @{
*/

/*!
	@anchor		accessoryDidConnect
	@brief      Called when the Companion is availaible. Use isKindOf to know which Companion was connected.
    <p align="justify">
        When this method is triggered, the device is ready to communicate with the payment module.<br />
        The @ref accessoryDidConnect event is fired each time that the channel is opened. It is not necessary to do any further initialization of the device, since it connects automatically to an external accessory using the same protocol whenever it is detected. The <b>sender</b> parameter informs the delegate about the channel that has just been activated so that it can perform any related process accordingly.
    </p>
	@param		 sender A pointer to the object that fired the @ref accessoryDidConnect event
*/
-(void)accessoryDidConnect:(ICISMPDevice*)sender;

/*!
	@anchor	 accessoryDidDisconnect
	@brief   Called when the Companion is disconnected. Use isKindOf to know which Companion was disconnected
    <p align="justify">
        This event is fired once even if many @ref ICISMPDevice channels have been opened to the external payment module. It notifies the delegate that the communication with the device will be interrupted. It is, then, up to the application to decide what should happen thereafter; display a disconnection notification to the user and tell him to reconnect and reboot the device, or just tell him to wait until the device resets if it is the case.
    </p>
	@param		 sender A pointer to the object that fired the @ref accessoryDidDisconnect event
*/
-(void)accessoryDidDisconnect:(ICISMPDevice *)sender;

/*! 
    @}
*/

@optional 

/*!
    @addtogroup   ICDeviceTraceLogging
    @{
*/

// log : you may implement either logEntry / logSerialData or implement the device specific --> only one call is made.

/*!
    @brief      Provides log traces and their severities
    <p>
        This callback may be called by the library from an arbitray thread. So the application should not perform a long or a blocking task within this callback.
    </p>
    @param      message     The logged message
    @param      severity    The severity of the logged message (one of the values defined within @ref SEVERITY_LOG_LEVELS)
*/
-(void) logEntry:(NSString*)message withSeverity:(int)severity;

/*!
    @brief      Provides traces of serial data exchanged with the terminal
    <p>
        This callback may be called by the library from an arbitray thread. So the application should not perform a long or a blocking task within this callback.
    </p>
    @param      data        The serial data
    @param      isIncoming  YES if the data comes to the iDevice, NO in the other case
*/
-(void) logSerialData:(NSData*)data incomming:(BOOL)isIncoming;

/*!
 @}
 */

@end

/*! 
    @}
*/

