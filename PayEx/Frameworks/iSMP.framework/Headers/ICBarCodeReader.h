//
//  ICBarCodeReader.h
//  PCL Library
//
//  Created by Christophe Fontaine on 21/06/10.
//  Copyright 2010 Ingenico. All rights reserved.
//

/*!
    @file       ICBarCodeReader.h
    @brief      Header file for ICBarCodeReader class and related data structures and protocols
 */

#import <Foundation/Foundation.h>
#import "ICISMPDevice.h"

#pragma mark -
#pragma mark ICBarCodeReaderDelegate


/*!
    @defgroup     ICBarCodeReaderGroup  Scanner Management
    @brief        This category involves all required methods and data structures to control the Companion scanner.
    @{
*/


/*!
    @protocol	  ICBarCodeReaderDelegate
    @brief        The BarcodeReader Channel's delegate methods
    <p align="justify">
        These methods should be implemented by the @ref ICBarCodeReader's delegate in order to be notified of its events. Only "barcodeData" method is required.<br />
        You should also implement the <b>unsuccessfulDecode</b> method to alert the user when the scanner fails to decode a barcode.
    </p>
*/

@protocol ICBarCodeReaderDelegate
@required

/*!
    @defgroup   ICBarCodeReaderScanningCallbacks Barcode Scanning Callbacks
    @ingroup    ICBarCodeReaderGroup
    @brief      All callbacks related to scanning and decoding barcodes
    <p>Those callbacks are defined within the @ref ICBarCodeReaderDelegate and received by the delegate of the @ref ICBarCodeReader object</p>
    @{
*/

/*!
    @anchor     barcodeData
    @brief      Callback received by the delegate of @ref ICBarCodeReader each time the scanner successfully decodes a barcode
    <p>
        The ICBarcodeData objects will contain ONLY the successfully decoded barcodes.
    </p>
    @param  data An object of type NSString if the scanner is configured to read barcodes, and of type UIImage if the scanner is used in snapshot
    @param  type The type of the barcode. This is one of the constants defined within @ref eICBarCodeSymbologies
*/
-(void)barcodeData:(id)data ofType:(int)type; // data : NSString*, type: eICBarCodeSymbologies


/*!
    @}
*/


/*!
    @defgroup   ICBarCodeReaderConfigurationCallbacks Barcode Scanner Configuration Callbacks
    @ingroup    ICBarCodeReaderGroup
    @brief      All callbacks that are related to the scanner's configuration.
    <p>Those callbacks are defined within the @ref ICBarCodeReaderDelegate and received by the delegate of the @ref ICBarCodeReader object</p>
    @{
*/

/*!
    @anchor     configurationRequest
    @brief      Called in the @ref ICBarCodeReader delegate when the scanner must be configured
    <p>
        When the channel is opened (after the powerOn method or a soft reset, ...), the scanner sends the powerup event, and requires to be configured
    </p>
*/
-(void)configurationRequest;

/*! 
    @}
*/

@optional

/*!
    @addtogroup ICBarCodeReaderScanningCallbacks
    @{
*/

/*!
    @anchor     unsuccessfulDecode
    @brief      Called on the delegate of @ref ICBarCodeReader when a barcode is read but the decoding failed
*/
-(void)unsuccessfulDecode;

/*!
    @anchor     triggerPulled
    @brief      Called on the delegate of @ref ICBarCodeReader when the hardware line of the trigger is pressed
*/
-(void)triggerPulled;


/*!
    @anchor     triggerReleased
    @brief      Called on the delegate of @ref ICBarCodeReader when the hardware line of the trigger is released
*/
-(void)triggerReleased;

/*!
    @anchor     onScanMiscEvent
    @brief      Other events coming from the barcode
    <p>
        These events have not been defined yet, they include :
        <ul>
            <li>Decoding events : unsuccessful decoding, start of read session, end of read session</li>
            <li>Hardware events : start-up, setup modification by reading a configuration barcode, configuration barcode rejected</li>
        </ul>
    </p>
    param eventCode An event code constant [NOT USED]
*/
-(void)onScanMiscEvent:(int)eventCode; // other scan events

/*! 
    @}
*/


/*!
    @defgroup   ICBarCodeReaderTraceLogging     ICBarCodeReader Trace Logging
    @ingroup    ICDeviceTraceLogging
    @brief      Trace Logging callbacks for @ref ICBarCodeReader class
    <p>Those callbacks are defined within the @ref ICBarCodeReaderDelegate and received by the delegate of the @ref ICBarCodeReader object</p>
    @{
*/

/*!
    @anchor        barcodeLogEntry:withSeverity:
    @brief		Method to be implemented by the delegate of @ref ICBarCodeReader in order to receive all non-serial log messages
    <p>
        Log messages may be filtred based on their severity
    </p>
    @param			logEntry The log message
    @param			severity The severity of the log
*/
- (void)barcodeLogEntry:(NSString *)logEntry withSeverity:(int)severity;


/*!
    @anchor        barcodeSerialData:incoming:
    @brief		Method to be implemented by the delegate of @ref ICBarCodeReader in order to receive the serial data passing through the barcode reader channel
    @param			data The serial data
    @param			isIncoming equal to YES when the data comes from the Companion
*/
- (void)barcodeSerialData:(NSData *)data incoming:(BOOL)isIncoming;

/*! 
    @}
*/

@end

#pragma mark -
/*! 
    @brief    Barcode Reader management class, derives from @ref ICISMPDevice
    <p>
        This class handles all the configuration and events of the device's barcode scanner
    </p>
*/

@interface ICBarCodeReader : ICISMPDevice {
	NSMutableArray					*multiCodeStrings;          /**< List of scanned barcode in  scan mode multiScan soft */
	
	NSCondition						*iscpResultReceived;        /**< Condition for received result */
	NSCondition						*iscpReplyReceived;         /**< Condition for reply result */
	NSCondition						*scannerStartedCondition;   /**< Condition for received startup event from barcode reader */
	
	NSData							*lastResultData;            /**< ISCP last result data */
	NSOperationQueue				*operationQueue;            /**< operation queue */
	
	int								bufferingSetupWrites;       /**< command counter */
	NSMutableArray					*setupWriteList;            /**< buffer of ISCP commands */
	
@private	
	// cached data
	NSInteger						currentScanMode;
	NSInteger						multiScan;	
	NSInteger						triggerEnabled;
	NSInteger						turboModeEnabled;
	NSInteger						currentImagerMode;
	NSString						*firmwareVersion;
    BOOL                            shouldRequestConfiguration;
    NSMutableData                   *picture;
    NSInteger                       sizeOfPicture;
}


#pragma mark BarcodeConstants



#pragma mark -

/*!
    @defgroup   ICBarCodeReaderInitialization   ICBarCodeReader Initialization
    @ingroup    ICBarCodeReaderGroup
    @brief      Steps to initialize ICBarCodeReader object
    <p>
        To initialize an ICBarCodeReader object, proceed as the following:
        <ul>
            <li>Retain the auto-released instance returned by @ref sharedICBarCodeReader</li>
            <li>Set its @ref barcodeDelegate property to a valid object that implements the @ref ICISMPDeviceDelegate and @ref ICBarCodeReaderDelegate</li>
            <li>Call @ref powerOn to turn on the scanner and start using it</li>
        </ul>
    </p>
*/


/*!
    @anchor     barcodeDelegate
    @brief      The delegate object that should implement the @ref ICBarCodeReaderDelegate protocol
    <p>
        By implementing the protocol, the delegate is notified with @ref ICBarCodeReader's events
    </p>
*/
@property(nonatomic, weak) id<ICISMPDeviceDelegate, ICBarCodeReaderDelegate> delegate;

/*!
    @brief   Defines the number of barcode frame resent
    <p>
        <ul><li>timeout of ACK frame is 2s</li><li>default value is DEFAULT_ISCP_RETRY_COUNT (5)</li></ul>
    </p>
*/
@property(nonatomic, assign) int iscpRetryCount;


/*!
    @anchor  sharedICBarCodeReader
    @brief   Returns the unique shared instance of @ref ICBarCodeReader
    <p align="justify">
				This initializer returns an autoreleased object that must be retained to be kept alive. The scanner is not started when initializing an @ref ICBarCodeReader object. This can be done by explicitely calling @ref ICBarCodeReader's @ref powerOn method that is responsible for opening the barcode channel to the connected device. The channel is closed when calling @ref powerOff or when the object is released.<br />
                Scanner state, whether it is ready or not, can be determined at any time by examing the value of the @ref isAvailable property. If the scanner is not available, all the other calls to control or configuration commands will fail. Therefore, the application should always check the @ref isAvailable property before sending any command.
			  </p>
    @result	 A pointer to the shared instance
*/
+(ICBarCodeReader *)sharedICBarCodeReader;

/*!
    @}
*/


/*!
    @defgroup   ICBarCodeReaderScanning Barcode Scanning
    @ingroup    ICBarCodeReaderGroup
    @brief      All methods provided by @ref ICBarCodeReader to perform software scanning of barcodes
    @{
*/

/*!
    @brief   Start a decoding session
    <p>
        The decoding session will remain active until a successfully decode, a timeout or when you call @ref stopScan.
    </p>
*/
-(void)startScan;


/*!
    @brief   Stop a decoding session
    <p>
        This method cancels the decoding session before reaching the timeout.<br />
        Please refer to <b>setScanTimeout</b> for more information.
    </p>
*/
-(void)stopScan;

/*!
 @brief   Start a snapshot image
 <p>
 not implemented<br />
 </p>
 */
-(void)startSnapshot;

/*!
    @}
*/


#pragma mark Configuration related functions

/*!
    @defgroup   ICBarCodeReaderConfiguration    Companion Scanner Configuration
    @ingroup    ICBarCodeReaderGroup
    @brief      Configuration functions of the Companion scanner
    @{
*/

/*!
    @anchor	 bufferWriteCommands
    @brief   Disable the setupwrite message, and buffer the configuration instead
    <p>
        The application MUST call @ref unbufferSetupCommands when it wishes to send the whole configuration
    </p>
*/
-(void)bufferWriteCommands;


/*!
    @anchor	 unbufferSetupCommands
    @brief   Reenable the setupwrite message, and send ALL buffered setup command in one message
    <p>
        A call to this selector whithout a previous call to @ref bufferWriteCommands has no effect
    </p>
*/
-(void)unbufferSetupCommands;

/*!
    @brief   To switch from single scan, multi scan, or raw mode
    <p>
        In singleScan mode, the delegate function will be called when a barcode is read. In multiScan mode, the scanner will decode barcodes until the trigger is released or @ref stopScan is called.
    </p>
    @param   mode The barcode reader mode to be activated. One of the constants of @ref eICBarCode_ScanMode
*/ 
-(void)configureBarCodeReaderMode:(int)mode; // for the mode, refers to eICBarCode_ScanMode


/*!
    @brief   Get the current barcode reader mode
    <p>
        Please refer to configureBarCodeReaderMode
    </p>
    @result	An integer of the @ref eICBarCode_ScanMode constants (singlescanMode or MultiscanMode)
*/ 
-(int)getBarCodeReaderMode; // see eICBarCode_ScanMode


/*!
    @anchor  isSymbologySupported
    @brief   Test whether the symbology is supported
    @param   type The symbology to be tested. One of the constants of @ref eICBarCodeSymbologies
    @result	 returns YES if the symbology is supported
*/
+(BOOL)isSymbologySupported:(int)type; // type: eICBarCodeSymbologies


/*!
    @anchor enableSymbologies
    @brief	Enable a list of symbologies
    <p>
        The array must contains NSInteger corresponding to the symbologies you wish to enable (@ref eICBarCodeSymbologies) set symbologies to NULL or define symbologyCount to 0 to disable all symbologies.
    </p>
    @param symbologies  An integer array of scanner barcode types defined within the @ref eICBarCodeSymbologies enumeration
    @param count        The total number of barcode types contained within the symbologies array
*/
-(void)enableSymbologies:(int *)symbologies symbologyCount:(int)count; // symbologies: int[] containing eICBarCodeSymbologies

/*!
    @anchor  enableSymbology
    @brief   Enable or disable a specific symbology
    @param   type The symbology to be enabled/disabled. Refer to @ref eICBarCodeSymbologies enumeration for all possible values
    @param	 enabled The new state to apply to the symbology
*/
-(void)enableSymbology:(int)type enabled:(BOOL)enabled; // type: eICBarCodeSymbologies 


/*!
    @anchor  isSymbologyEnabled
    @brief   Determine whether the decoding of the specific symbology is enabled
    <p>
        This method tells whether a symbology is enabled by the scanner. It is synchronous and may be time-consuming in case the scanner is busy doing 
			 other stuff. It should be better performed in a background thread.
    </p>
    @param   type A symbology constant defined within @ref eICBarCodeSymbologies enumeration
    @result	 returns TRUE if the symbology is enabled
*/
-(BOOL)isSymbologyEnabled:(int)type; // type: eICBarCodeSymbologies


/*!
    @anchor  enableTransmitUPCABarcodesAsEAN13
    @brief   Enable transmission of UPC-A barcodes as EAN-13
    <p>
        EAN 13 is an extended form of UPC-A and adds a country code. The Companion scanner can be configured using this method to convert and transmit UPC-A barcodes in the EAN-13 format. The enabled argument should be set to YES to enable the conversion, to NO to send UPC-A barcodes as they are. By default, the Companion scanner is configured to not transmit UPC-A barcodes as EAN-13.
    </p>
    @param   enabled Boolean that should be set to YES to enable transmission of UPC-A barcodes as EAN-13, NO to disable this conversion.
*/
-(void)enableTransmitUPCABarcodesAsEAN13:(BOOL)enabled;


/*!
    @anchor  enableTransmitUPCEBarcodesAsUPCA
    @brief   Enable transmission of UPC-E barcodes as UPC-A
    <p>
        UPC-E is a variation of UPC-A which allows for a more compact barcode by eliminating "extra" zeros. Since the resulting UPC-E barcode is about half the size of an UPC-A barcode, UPC-E is generally used on products with very small packaging where a full UPC-A barcode couldn't reasonably fit.<br />
        This function allows decoded UPC-E barcodes to be converted and transmitted as UPC-A.
    </p>
    @param   enabled Boolean that should be set to YES to enable transmission of UPC-E barcodes as UPC-A, NO to disable this conversion.
*/
-(void)enableTransmitUPCEBarcodesAsUPCA:(BOOL)enabled;


/*!
    @anchor  enableTransmitEAN8BarcodesAsEAN13
    @brief   Enable transmission of EAN 8 barcodes as EAN-13
    <p>
        EAN-8 is the EAN equivalent of UPC-E in the sense that it provides a "short" barcode for small packages. An EAN-8 barcode is shorter than an EAN-13 barcode, although somewhat longer than an UPC-E barcode.<br />
        This function allows decoded EAN-8 barcodes to be converted and transmitted as EAN-13.
    </p>
    @param   enabled Boolean that should be set to YES to enable transmission of EAN-8 barcodes as EAN-13, NO to disable this conversion.
*/
-(void)enableTransmitEAN8BarcodesAsEAN13:(BOOL)enabled;


/*!
    @anchor	 eICBarCodeSymbologies
    @brief   An enumeration of the symbologies supported by the barcode scanner
    <p>
        Those constants are used by the @ref enableSymbology, @ref enableSymbologies, @ref isSymbologyEnabled and @ref isSymbologySupported methods
    </p>
*/
enum eICBarCodeSymbologies {
	ICBarCode_Unknown = -1,                     /**< Unknown Symbology */
	ICBarCode_AllSymbologies	= 0,            /**< All Symbologies */
	
	ICBarCode_EAN13,                            /**< EAN13 Barcode Type */
	ICBarCode_EAN8,                             /**< EAN8 Barcode Type */
	ICBarCode_UPCA,                             /**< UPCA Barcode Type */
	ICBarCode_UPCE,                             /**< UPCE Barcode Type */
	
	ICBarCode_EAN13_2,                          /**< EAN13_2 Barcode Type */
	ICBarCode_EAN8_2,                           /**< EAN8_2 Barcode Type */
	ICBarCode_UPCA_2,                           /**< UPCA_2 Barcode Type */
	ICBarCode_UPCE_2,                           /**< UPCE_2 Barcode Type */
	
	ICBarCode_EAN13_5,                          /**< EAN13_5 Barcode Type */
	ICBarCode_EAN8_5,                           /**< EAN8_5 Barcode Type */
	ICBarCode_UPCA_5,                           /**< UPCA_5 Barcode Type */
	ICBarCode_UPCE_5,                           /**< UPCE_5 Barcode Type */
	
	ICBarCode_Code39,                           /**< Code39 Barcode Type */

	ICBarCode_Interleaved2of5 = 15,             /**< Interleaved2of5 Barcode Type */
	ICBarCode_Standard2of5,                     /**< Standard2of5 Barcode Type */
	ICBarCode_Matrix2of5,                       /**< Matrix2of5 Barcode Type */
	
	ICBarCode_CodaBar = 19,                     /**< CodeBar Barcode Type */
	ICBarCode_AmesCode,                         /**< AmesCode Barcode Type */
	ICBarCode_MSI,                              /**< MSI Barcode Type */
	ICBarCode_Plessey,                          /**< Pleassey Barcode Type */
	
	ICBarCode_Code128,                          /**< Code128 Barcode Type */
	ICBarCode_Code16K,                          /**< Code16k Barcode Type */
	ICBarCode_93,                               /**< Code93 Barcode Type */
	ICBarCode_11,                               /**< Code11 Barcode Type */
    
	ICBarCode_Telepen,                          /**< Telepen Barcode Type */
	ICBarCode_Code49,                           /**< Code49 Barcode Type */
	ICBarCode_Code39_ItalianCPI,                /**< Code39_ItalianCPI Barcode Type */
	
	ICBarCode_CodaBlockA,                       /**< CodeBlockA Barcode Type */
	ICBarCode_CodaBlockF,                       /**< CodaBlockF Barcode Type */
	
	ICBarCode_PDF417 = 33,                      /**< PDF417 Barcode Type */
	ICBarCode_GS1_128, // Replace EAN128        /**< GS1_128 Barcode Type */
	ICBarCode_ISBT128,                          /**< ISBT128 Barcode Type */
	ICBarCode_MicroPDF,                         /**< MicroPDF Barcode Type */
    
    ICBarCode_GS1_DataBarOmni,                  /**< GS1_DataBarOmni Barcode Type */
    ICBarCode_GS1_DataBarLimited,               /**< GS1_DataBarLimited Barcode Type */
    ICBarCode_GS1_DataBarExpanded,              /**< GS1_DataBarExpanded Barcode Type */
    
	ICBarCode_DataMatrix,                       /**< DataMatrix Barcode Type */
    ICBarCode_QRCode,                           /**< QRCode Barcode Type */
	ICBarCode_Maxicode,                         /**< Maxicode Barcode Type */
    ICBarCode_UPCE1,                            /**< UPC-E1 Barcode Type */
    
    ICBarCode_Aztec = 0x4A,                     /**< Aztec Barcode Type */
	
	
	ICBarCode_MaxIndex                          /**< MaxIndex Barcode Type */
};



/*!
    @anchor	 eICBarCode_ScanMode
    @brief   An enumeration of the scan Modes
    <p>
        These constants are to be used within @ref configureBarCodeReaderMode: function
    </p>
*/
enum eICBarCode_ScanMode {
	ICBarCodeScanMode_SingleScan,               /**< Single-Scan Mode */
	ICBarCodeScanMode_MultiScan                 /**< Multi-Scan Mode */
};

/*!
    @anchor        eICBarCode_ImagerMode
    @brief         An enumeration of the predefined imager mode
    <p>
        These constants are to be used within @ref configureImagerMode: method
    </p>
*/
enum eICBarCode_ImagerMode {
	ICBarCodeImagerMode_1D,                     /**< 1D */
	ICBarCodeImagerMode_1D2D,                   /**< 1D and 2D standard */
	ICBarCodeImagerMode_1D2D_bright,            /**< 1D and 2D bright environment */
	ICBarCodeImagerMode_1D2D_reflective         /**< 1D and 2D reflective surface */
};

#pragma mark -
#pragma mark Imager Configuration
/*!
    @brief   Select a predefined imager mode
    <p>
        Predefined modes are 1D, 1D&2D, 1D&2D bright environnement and 1D&2D reflective surface
    </p>
    @param  mode Integer value that must be one of the constants defined within the @ref eICBarCode_ImagerMode enumeration
 */ 	
-(void)configureImagerMode:(int)mode; // mode : refers to eICBarCode_ImagerMode


/*!
    @brief  Enable or disable the flashing of the aimer
    @param  enabled Boolean argument that represents the desired state of the aimer flashing property
 */
-(void)enableAimerFlashing:(BOOL)enabled;


/*!	
	@anchor		eICBarCode_IlluminationMode
    @brief      An enumeration of available illuminiation modes
*/
enum eICBarCode_IlluminationMode {
	aimerAndIlluminiationLeds,              /**< Enable both the aimer and the leds */
	aimerOnly,                              /**< Enable only the aimer */
	illuminationLedsOnly,                   /**< enable only the leds */
	noIllumination                          /**< Disable illumination */
};


/*!
    <p>
        configures the barcode illumination mode, according the the enum @ref eICBarCode_IlluminationMode
    </p>
    @param mode An interger value of the illumination mode to be set. All possible values are defined within the @ref eICBarCode_IlluminationMode enumeration.
*/
-(void)illuminationMode:(int)mode; // mode: eICBarCode_IlluminationMode


/*!
    @brief  Defines the maximum brightness (percentage, from 0 to 100%)
    <p align="justify">
        Set the maximum brightness level of the illumination LEDs that can be used to achieve the lighting goal. The parameter value corresponds to a percentage from 0 to 100% (0% = off, 100% = brightest).</p><p>Note: Set the illumination level on a lower setting when reading shiny barcode labels where high intensity lighting will be too bright to decode the label.
    </p>
    @param  level Integer value in the range [0, 100] of the illumination level to be set.
 */
-(void)illuminationLevel:(int)level;



/*!
    @brief   Target average light intensity, from 0 to 255
    <p>
        The lighting goal is the average light intensity that the scanner tries to achieve when capturing an image.<br />If the lighting goal is set too low, the image will be dark (underexposed).<br />If it is set too high the image will be too bright (overexposed).<br />Use the lighting mode setting to determine how the scanner tries to acheive the lighting goal.
    </p>
    @param  goal An integer value in the range [0, 255]   
 */
-(void)lightingGoal:(int)goal;


/*!
	@anchor		eICBarCode_LightingMode
    @brief      An enumeration of the scanner's lighting modes
*/
enum eICBarCode_LightingMode {
	illuminiationLEDPriority,       /**< Shorter exposure time  */
	aperturePriority                /**< Use aperture priority if you have a shiny barcode label */
};

/*!
    @brief  LightingGoal mode : illumination priority or aperture priority
    <p align="justify">
        The lighting mode determines how the scanner tries to acheive the lighting goal. Priority can be given to the illumination LEDs or to aperture (exposure time). Using the illum LED priority setting results in shorter exposure time, therefore less risk of blurred images.<br />
        Use the aperture priority setting when you have a shiny barcode label where high intensity lighting will be too bright to decode it.
    </p>
    @param  priorityType   Must be one of the values defined within @ref eICBarCode_LightingMode enumeration.
 */
-(void)lightingMode:(int)priorityType; // priorityType: eICBarCode_LightingMode


/*!
    @brief  Returns the aimer flashing state
    @result	YES if the aimar flashing is enabled, NO otherwise
 */
-(BOOL)aimerFlashing;


/*!
    @brief   Get the current illumination mode of the barcode scanner
    @result	 An integer representing one of the illumination modes defined in @ref eICBarCode_IlluminationMode
 */
-(int)illuminiationMode;


/*!
    @brief   Gets the current illumination level
    @result	 Integer between 0 and 100 representing the illumination level of the barcode reader's light
 */
-(int)illuminationLevel;


/*!
    @brief   Gets the current lighting goal of the barcode scanner
    @result	 Integer ranging from 0 to 255
 */
-(int)lightingGoal;


/*!
    @brief   Gets the lighting mode of the barcode scanner
    @result	 Integer representing one of the lighting modes defined in @ref eICBarCode_LightingMode
 */
-(int)lightingMode;

/*!
 @brief  Enable or disable the Damaged 1D Codes option. Enable this function when scanning damaged or badly printed 1D barcodes. This setting enhances the ability to read these types of bar codes
 @param  enabled Boolean argument that represents the desired state of the Damaged 1D Codes
 */
-(void)useEnhancedBCRSensitivity:(BOOL)enabled;

/*!
 @brief  Returns the Damaged 1D function state.
 @result	YES if the Damaged 1D is enabled, NO otherwise
 */
-(BOOL)isEnhancedBCRSensitivityEnabled;

/*!
    @}
*/


#pragma mark -
#pragma mark Power Mgmt

/*!
    @defgroup   ICBarCodeReaderPowerManagement  Scanner Power Management
    @ingroup    ICBarCodeReaderGroup
    @brief      Power management functions of the Companion scanner
    @{
*/

/*!
    @anchor  powerOn
	@brief   Power on the scanner
	<p align="justify">
        This method powers on the scanner of the Companion terminal. It may take up to 1 second for this function to return. The powerOn request is not always successful. It may fail if there is any synchronization problem or when the Companion is being charged on the craddle.<br />
        After the scanner is started, the @ref configurationRequest callback is called to invite the user application to apply its scanner configuration (set single or multi-scanning, choose the barcode symbologies, choose the imager mode 1D/2D, ...).<br />
    </p>
    @result One of the constants defined in @ref eICBarCode_PowerOnStatus according to whether the call was successful or not.
*/
-(int)powerOn;

/*!
    @anchor     eICBarCode_PowerOnStatus
    @brief      An enumeration of the @ref powerOn status
    <p>
        The @ref powerOn function that starts the scanner returns one of the following value.
    </p>
*/
enum eICBarCode_PowerOnStatus {
    ICBarCodeReader_PowerOnSuccess,             /**< The powerOn command was successful */
    ICBarCodeReader_PowerOnFailed,              /**< The powerOn command failed due to a synchronization problem */
    ICBarCodeReader_PowerOnDenied               /**< The powerOn command was forbidden. This happens when the device is charging on the craddle */
};


/*!
    @anchor  powerOff
	@brief   Power off the scanner
	<p align="justify">
        The barcode scanner should be turned off when not in use to save the device's battery life.
    </p>
*/
-(void)powerOff;



/*!
    @brief   Soft-Reset the barcode scanner
    <p>
        The soft-reset reinitializes the scanner parameters
    </p>
 */
-(void)softReset;

/*!
 @brief   Apply the default configuration of the barcode scanner
 <p>
 This method reinitializes the scanner parameters to the default configuration
 </p>
 */
-(void)applyDefaultConfiguration;

/*!
    @brief   Defines the delay after which the scanner will abort the barcode decoding
    @param		 timeout The new scan timeout to apply.
*/
-(void)setScanTimeout:(int)timeout; // Timeout in second, from 0 (timeout disabled) to 60 seconds


/*!
    @brief   Get the scan timeout of the barcode reader
    <p>
        This method is synchronous and blocking. It returns the timeout value in seconds. The returned value is between 0 (no timeout) and 60 seconds.
    </p>
    @result	 int An integer containing the value of the timeout, or -1 if a timeout error occured
*/
-(int)getScanTimeout; // -1 if an error occured (timeout)

/*!
 @brief   Enable / Disable non volatile mode
 <p>
 When using non volatile mode, parameters are restored when the barcode reader is opened.
 </p>
 @param	enabled TRUE to set non volatile mode, FALSE to set volatile mdoe
 */
-(void)setNonVolatileMode:(BOOL)enabled;

/*!
    @}
*/

#pragma mark -
#pragma mark Misc

/*!
    @addtogroup   ICBarCodeReaderConfiguration
    @{
*/

// Misc
/*!    
    @brief   Converts symbology code to its text representation.
    @param	 type A barcode type. All the symbologies are defined within @ref eICBarCodeSymbologies
    @result	 an NSString containing the name of the symbology
*/
+(NSString*)symbologyToText:(int)type; // type: eICBarCodeSymbologies

/*!
    @brief   Enable / Disable beep
    <p>
        Use this method if you wish to enable/disable the beep when a barcode is successfully read
    </p>
    @param		 enabled A boolean value to enable/disable the good scan beep
*/


-(void)goodScanBeepEnable:(BOOL)enabled;
/*!
    @brief   Configure beep features
    <p>
        This method changes the scan and error beep length and frequency
    </p>
    @param		 enabled A boolean value to enable or disable the beep sound
    @param		 frequency : from 1000 to 4095 kHz
    @param		 length : in ms
*/
-(void)setBeep:(BOOL)enabled frequency:(int)frequency andLength:(int)length;


/*!
    @brief   play the frequency (from 1000 to 4095) for "onTimeMs" ms then wait "offTimeMs" ms
    <p>
        This is a non-blocking method. Call it sequencially in order to play a sequence of "beeps".
    </p>
    @param		 frequency The beep sound frequency
    @param		 onTimeMs The duration in milliseconds of the beep
    @param		 offTimeMs Silence in milliseconds (0 to 4095 ms) after the beep. 
*/
-(void)playBeep:(int)frequency during:(int)onTimeMs andWait:(int)offTimeMs;


/*!
    @anchor  enableTrigger
    @brief   Enable or disable the trigger button of the barcode reader
    @param   enabled If set to YES, the trigger keys are enabled, otherwise, they are disabled.
    @result	 A boolean indicating wether the command succeded or not. If equal to YES, then the trigger enable/disable request was executed properly.
*/
-(BOOL)enableTrigger:(BOOL)enabled;


/*!
    @anchor  isTriggerEnabled
    @brief   Retrieve the trigger state of the barcode scanner
    <p>
        This is a synchronous and blocking method. It should be better run in background since it may be time-consuming
    </p>
    @result	 The trigger's enabled/disabled state
*/
-(BOOL)isTriggerEnabled;

/*! 
    @}
*/


/*!
    @defgroup   ICBarCodeReaderInformation Scanner Information
    @ingroup    ICBarCodeReaderGroup
    @brief      Methods to retrieve information about the scanner
    @{
*/

/*! 
	@brief   Retrieve the barcode reader's firmware version
    <p>
        This method is synchronous and may take some time before it gets the result. It should be run in a seperate thread in order not to freeze the 
				main thread.
    </p>
	@result	 A NSString containg the version string of the barcode reader's firmware
*/
-(NSString *)getFirmwareVersion; // returns the barcode firmware version


/*!
	@brief   Returns the barcode scanner name
	@result	 A NSString containg the scanner name
*/
-(NSString *)scannerName;


/*!
	@brief      Returns the barcode scanner model
	@result		A NSString containg scanner model
*/
-(NSString *)scannerModel;

/*! 
    @}
*/

#pragma mark -

@end 

/*!
    @}
*/
