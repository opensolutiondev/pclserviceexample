//
//  iBP.h
//  PCL
//
//  Created by Hichem Boussetta on 02/01/12.
//  Copyright (c) 2012 Ingenico. All rights reserved.
//


/*!
    @file       iBP.h
    @brief      This file includes all iBP Library's header files
    <p>The included files are :
        <ul>
            <li>ICAdministration+iBP.h</li>
        </ul>
        This framework requires the iSMP.framework as a dependency since it extends the ICAdministration class.
    </p>
 
*/


#import "ICAdministration+iBP.h"
#import "iBPBitmapContext.h"
