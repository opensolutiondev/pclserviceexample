//
//  TerminalCommunicationManager
//  PayEx
//
//  Created by Anton Zhigalov on 08.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation

protocol TerminalCommunicationManager {
    func sendMessage(message: Message,
                     success: @escaping ((Message) -> Void),
                     failure: @escaping ((CommunicationError) -> Void),
                     finished: @escaping (() -> Void))

    func disconnect()
    func isBusy() -> Bool
}
