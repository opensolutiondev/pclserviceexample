//
//  CommunicationErrors.swift
//  PayEx
//
//  Created by Anton Zhigalov on 15.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation

public enum CommunicationError: Error {
    case noConnection
    case parsingError
    case failedStreamInit
    case failedMessageSend
    case failedReadingMessage
    case internalError(Error?)

    var localizedDescription: String {
        switch self {
        case .noConnection:
            return NSLocalizedString("No connection", comment: "")
        case .parsingError:
            return NSLocalizedString("Failed to parse the data received from the terminal", comment: "")
        case .failedStreamInit:
            return NSLocalizedString("Failed to init or open input/output streams", comment: "")
        case .failedMessageSend:
            return NSLocalizedString(
                """
                Failed to send a message to the terminal.
                Could not serialize the message or writing to the stream has failed
                """,
                                     comment: "")
        case .failedReadingMessage:
            return NSLocalizedString(
                """
                Failed to read the data from the terminal.
                Input stream is not ready or reading stream data has failed
                """,
                                     comment: "")

        case .internalError(let error):
            if let error = error {
                return NSLocalizedString("Internal error", comment: "") + ": \(error.localizedDescription)"
            }

            return NSLocalizedString("Internal terminal error", comment: "")
        }
    }
}
