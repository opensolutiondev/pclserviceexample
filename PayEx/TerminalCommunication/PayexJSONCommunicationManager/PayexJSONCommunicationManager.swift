//
//  PayexJSONCommunicationManager
//  PayEx
//
//  Created by Anton Zhigalov on 08.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation
import iSMP

private let kPort: Int = 9599
private let kReadBufferSize: Int = 1024
private let kTerminalHost = "127.0.0.1"

class PayexJSONCommunicationManager: NSObject, TerminalCommunicationManager, StreamDelegate {
    //Statuses returned when the bridge has been added successfully
    private enum BridgeStates: Int32 {
        case successful = 0
        case alreadyExists = -2
    }

    private var outputStream: OutputStream?
    private var inputStream: InputStream?

    private var success: ((Message) -> Void)!
    private var failure: ((CommunicationError) -> Void)!
    //When an operation has been finished (.endEncountered is reached for the strem) or disconnect() called
    private var finished: (() -> Void)!

    //Closures indicating the connection has been established: the bridge has been added, streams opened etc
    private var connectionSuccess: (() -> Void)!
    private var connectionFailure: ((CommunicationError) -> Void)!

    //Will compose the message returned from the terminal
    //By a few chunks if necessary
    private let mergeHelper = MergeHelper()

    deinit {
        disconnect()
    }

    // MARK: - TerminalCommunicationManager
    func sendMessage(message: Message,
                     success: @escaping ((Message) -> Void),
                     failure: @escaping ((CommunicationError) -> Void),
                     finished: @escaping (() -> Void)) {
        self.success = success
        self.failure = failure
        self.finished = finished

        connect(success: {[unowned self] in
            self.send(message: message)
        }, failure: { [unowned self] (error) in
            self.failure(error)
        })
    }

    func disconnect() {
        deinitStreams()

        if let finished = self.finished { //because it might be called at any time on the client
            finished()
        }
    }

    func isBusy() -> Bool {
        return ICAdministration.sharedChannel().isIdle() == false
    }

    // MARK: - StreamDelegate methods
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        switch eventCode {
        case .openCompleted:
            print("Stream opened")

            didOpen(stream: aStream)

        case .hasBytesAvailable:
            print("Has bytes available")

            didReceiveBytes(stream: aStream)

        case .errorOccurred:
            print("Error occured")

            didReceiveError(stream: aStream)

        case .endEncountered: //Communication finished, the streams will be closed
            print("End encountered")

            didFinish(stream: aStream)

        default:
            print("Unknown Event \(eventCode)")
        }
    }

    // MARK: - Private
    private func isTerminalReady() -> Bool { //Checks if PCLService is ready
        let state = ICPclService.shared().getState()
        let additionalCheck = ICPclService.shared().getTerminalInfo()

        return (state == PCL_SERVICE_STARTED || state == PCL_SERVICE_CONNECTED) && additionalCheck.serialNumber >= 0
    }

    //Preparing communication
    private func connect(success: @escaping () -> Void,
                         failure: @escaping (CommunicationError) -> Void) {
        if isTerminalReady() { //Checking phisical connection
            self.connectionSuccess = success
            self.connectionFailure = failure

            //There is no method to remove the bridge in the future
            let dynamicBridgeResult = ICPclService.shared().addDynamicBridgeLocal(kPort, 0)
            let bridgeState = BridgeStates(rawValue: dynamicBridgeResult)

            if bridgeState == .successful || bridgeState == .alreadyExists {
                if !initStreams() {
                    failure(.failedStreamInit)
                }
            }
        } else {
            failure(.noConnection)
        }
    }

    private func send(message: Message) {
        //Sending the message to the terminal by writing it to the stream
        guard let data = message.packed().data(using: .isoLatin1), write(data: data) > 0 else {
            self.failure(.failedMessageSend)

            return
        }
    }

    // MARK: Stream handling
    private func didOpen(stream: Stream) {
        //Both streams are ready and we can communicate with the terminal
        if isInputStreamReady() && isOutputStreamReady() {
            self.connectionSuccess()
        }
    }

    private func didReceiveBytes(stream: Stream) {
        //Got a package from the terminal.
        //The app will try to parse it to a Message instance
        if let input = self.inputStream, stream == input, let data = read(input: input) {
            //If result == nil, waiting for the next chunk of data
            if let result = self.mergeHelper.append(data: data) {
                switch result {
                case .success(let dictionary):
                    if let message = Message(json: dictionary) {
                        //send pong in the reply of ping
                        if message.messageType == "ping" {
                            send(message: Message(messageType: "pong", data: nil))
                        }

                        self.success(message)
                    } else {
                        self.failure(.parsingError)
                    }
                case .failure(let error):
                    self.failure(error)
                }
            }

            return
        } else {
            self.failure(.failedReadingMessage)
        }
    }

    private func didReceiveError(stream: Stream) {
        if let error = stream.streamError {
            let nserror = error as NSError
            print("TCP error: \(error.localizedDescription) (\(nserror.domain), code: \(nserror.code))")

            self.failure(.internalError(nserror))
        } else {
            print("TCP error: unknown error")

            self.failure(.internalError(nil))
        }

        disconnect()
    }

    private func didFinish(stream: Stream) {
        disconnect()
    }

    // MARK: - Helpers
    //Reading data from the stream recieved from the terminal
    private func read(input: InputStream) -> Data? {
        guard input.streamStatus == .open else {
            return nil
        }

        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: kReadBufferSize)
        var data = Data()

        while input.hasBytesAvailable {
            let read = input.read(buffer, maxLength: kReadBufferSize)
            if read > 0 {
                data.append(buffer, count: read)
            }
        }

        buffer.deallocate()

        return data
    }

    //Sending data to the terminal by writing bytes to the stream
    private func write(data: Data) -> Int {
        guard let output = self.outputStream, isOutputStreamReady() else {
            return 0
        }

        return data.withUnsafeBytes { output.write($0, maxLength: data.count) }
    }

    //Make sure the streams are ready to "rock" :)
    private func isOutputStreamReady() -> Bool {
        guard let output = self.outputStream, output.streamStatus == .open, output.hasSpaceAvailable else {
            return false
        }

        return true
    }

    private func isInputStreamReady() -> Bool {
        guard let input = self.inputStream, input.streamStatus == .open else {
            return false
        }

        return true
    }

    private func initStreams() -> Bool {
        //Using current streams if available
        if isInputStreamReady() && isOutputStreamReady() {
            self.connectionSuccess()

            return true
        }

        var tempInput: InputStream?
        var tempOutput: OutputStream?

        //Since we support iOS 8, used the new method to prepare the streams
        Stream.getStreamsToHost(withName: kTerminalHost, port: kPort, inputStream: &tempInput, outputStream: &tempOutput)

        guard let input = tempInput, let output = tempOutput else {
            return false
        }

        self.inputStream = input
        self.outputStream = output

        input.delegate = self
        output.delegate = self

        input.schedule(in: .current, forMode: .defaultRunLoopMode)
        output.schedule(in: .current, forMode: .defaultRunLoopMode)

        input.open()
        output.open()

        return true
    }

    private func deinitStreams() {
        if let input = self.inputStream {
            //            if input.streamStatus == .open {
            //                input.close()
            //            }
            input.close()

            input.remove(from: .current, forMode: .defaultRunLoopMode)
            self.inputStream = nil
        }

        if let output = self.outputStream {
            //            if output.streamStatus == .open {
            //                output.close()
            //            }
            output.close()

            output.remove(from: .current, forMode: .defaultRunLoopMode)
            self.outputStream = nil
        }
    }
}
