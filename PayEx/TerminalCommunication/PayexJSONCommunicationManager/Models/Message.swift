//
//  Message.swift
//  PayEx
//
//  Created by Anton Zhigalov on 15.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation

struct Message { //Not Codable because of using the data field as a Dictionary
    var version: Int = 1
    var messageType: String
    var data: [String: Any]?

    init(messageType: String, data: [String: Any]?) {
        self.messageType = messageType
        self.data = data
    }

    init?(json: [String: Any]) {
        guard let version = json["version"] as? Int,
            let messageType = json["message_type"] as? String else {
                return nil
        }

        self.version = version
        self.messageType = messageType

        if let data = json["data"] as? [String: Any] {
            self.data = data
        }
    }

    func packed() -> String {
        let serialized = serialize()

        return "\(serialized.count)\n\(serialized)"
    }

    // MARK: - Private
    func serialize() -> String {
        var dictionary: [String: Any] = ["version": version,
                                          "message_type": messageType]

        if let data = data {
            dictionary["data"] = data
        }

        if let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: []),
            let serialized = String(data: jsonData, encoding: .isoLatin1) {
            return serialized
        }

        return "" //Should never be reached
    }
}
