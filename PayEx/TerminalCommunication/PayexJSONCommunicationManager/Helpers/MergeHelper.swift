//
//  MergeHelper.swift
//  PayEx
//
//  Created by Anton Zhigalov on 21.05.2018.
//  Copyright © 2018 Fastdev. All rights reserved.
//

import Foundation

//This class will compose a dictionary object from the data received from the terminal
//The data may be delived by a few chunks, so the class will 'merge' those chunks into an object
class MergeHelper {
    //Length of the data returned
    private var length: Int = 0
    private var message: String = ""

    //If the message is complete, it will be returned as a dictionary, otherwise nil and will be handled the next chunk
    func append(data: Data) -> MergeResult? {
        guard let response = String(bytes: data, encoding: .isoLatin1) else {
            print("Can't convert the data into an isoLatin1 string")

            return MergeResult.failure(.parsingError)
        }

        print("Data to parse: " + response)

        let components = response.components(separatedBy: .newlines)

        if !components.isEmpty {
            if let len = Int(components[0]), len > 0, components.count >= 2 {
                self.length = len
                self.message = components[1]
            } else {
                if let first = components.first {
                    self.message += first
                }
            }
        }

        //It was the final chunk
        //Try to parse the whole message to a dictionary
        if self.message.count + 1 == self.length {
            if let messageData = self.message.data(using: .isoLatin1),
                let dictionary = (try? JSONSerialization.jsonObject(with: messageData, options: [])) as? [String: Any] {
                self.length = 0
                self.message = ""

                return MergeResult.success(dictionary)
            }
        }

        return nil
    }
}
